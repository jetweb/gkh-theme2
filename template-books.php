<?php
/**
 * Template Name: books
 *
 */
add_action('genesis_loop', 'productions_loop');
add_action('parse_query', function ($query) {
    $query->set('posts_per_page', 12);
});

function productions_loop()
{
    $paged     = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args      = ['post_type' => 'book', 'post_status' => 'publish', 'paged' => $paged];
    $the_query = new WP_Query($args);
    ?>
<div class="block-container productions-container" data-micromodal-trigger="modal">
    <div class="block-inner-container">
        <div class="productions-grid">
            <?php
$num = 1;
    while ($the_query->have_posts()) {
        $the_query->the_post();

        ?>
            <div class="production-item">
                <a data-fancybox='gallery' data-src="#hidden-content-<?php echo $num ?>" href="javascript:;">
                    <div class="production-item-inner" data-src="#hidden-content">
                        <div class="production-image-wrap">
                            <?php the_post_thumbnail('productions');?>
                        </div>
                    </div>
                </a>
                <div class="production-title"><?php the_title();?>
                </div>


            </div>

            <div style="display: none;" class='modal' id="hidden-content-<?php echo $num ?>">
                <div class="flex-item">
                    <div class="modal-image-wrap">
                        <?php the_post_thumbnail('productions');?>
                    </div>
                    <div class="modal-text-wrap">
                        <h2><?php the_title();?></h2>
                        <h3><?php the_field('sub_title');?></h3>
                        <div class="modal-content">
                            <?php the_content();?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
$num = $num + 1;
    }

    wp_reset_postdata();

    ?>

        </div>

        <div class="pagination-books pagination">
            <?php
echo paginate_links(array(
        'base'         => str_replace(999999999, '%#%', esc_url(get_pagenum_link(999999999))),
        'total'        => $the_query->max_num_pages,
        'current'      => max(1, get_query_var('paged')),
        'format'       => '?paged=%#%',
        'show_all'     => false,
        'type'         => 'plain',
        'end_size'     => 2,
        'mid_size'     => 1,
        'prev_next'    => true,
        'prev_text'    => sprintf('<i></i> %1$s', 'אחורה', 'text-domain'),
        'next_text'    => sprintf('%1$s <i></i>', 'הבא', 'text-domain'),
        'add_args'     => false,
        'add_fragment' => '',
    ));
    ?>
        </div>

    </div>
</div>

<?php

}
genesis();