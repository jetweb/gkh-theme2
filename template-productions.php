<?php
/**
 * Template Name: דף: הפקות מולטימדיה
 *
 */
add_action('genesis_loop', 'productions_loop');
function productions_loop()
{
    $args      = ['post_type' => 'production', 'post_status' => 'publish','posts_per_page' => '-1',];
    $the_query = new WP_Query($args);
    ?>
<div class="block-container productions-container" data-micromodal-trigger="modal">
    <div class="block-inner-container">
        <div class="productions-grid">
            <?php
$num = 1;
    while ($the_query->have_posts()) {
        $the_query->the_post();

        ?>
            <div class="production-item">
                <a data-fancybox data-src="#hidden-content-<?php echo $num ?>" href="javascript:;">
                    <div class="production-item-inner" data-src="#hidden-content">
                        <div class="production-image-wrap">
                            <?php the_post_thumbnail('productions');?>
                        </div>
                    </div>
                </a>
                <div class="production-title"><?php the_title();?>
                </div>


            </div>

            <div style="display: none;" class='modal' id="hidden-content-<?php echo $num ?>">
                <div class="flex-item">
                    <div class="modal-image-wrap">
                        <?php 
                        if(get_field('video_url')){
                            ?>
                            <video src="<?php the_field('video_url') ?>" controls></video>
                            <?php
                        }else{
                        the_post_thumbnail('productions');
                        }?>
                    </div>
                    <div class="modal-text-wrap">
                        <h2><?php the_title();?></h2>
                        <h3><?php the_field('sub_title');?></h3>
                        <div class="modal-content">
                            <?php the_content();?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
$num = $num + 1;
    }
    wp_reset_postdata();

    ?>

        </div>
    </div>
</div>

<?php

}
genesis();