<?php
/**
 * Archive partial
 *
 * @package      EAGenesisChild
 * @author       Bill Erickson
 * @since        1.0.0
 * @license      GPL-2.0+
 **/

echo '<article class="post-summary" video-url=' . get_field("video") . '>';
print_video_url();

echo '<div class="post-summary__content">';
ea_entry_category();
ea_visual_test_title();
echo '</div>';

echo '</article>';