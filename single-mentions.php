<?php
add_filter( 'genesis_post_title_output', 'remove_single_custom_post_titles' );
function remove_single_custom_post_titles(){
    return '<h2 role="heading" class="mention-header"> ב"גנזך קידוש השם" מציינים את…</h2>';
}

add_action('genesis_entry_content','print_title',0);
function print_title(){
?>
    <h1><?php the_title() ?></h1>
    <style>
        .header-text-wrap h2 {
    font-size: 81px;
}
h1 {
    font-size: 41px;
    text-decoration: underline #FFD65D;
    margin-top: 61px;
    margin-right: 0 !important;
}
    </style>
<?php
}
genesis();