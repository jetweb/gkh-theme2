<?php

/**
 * Template Name: דף: סיורים
 *
 */
add_action('genesis_entry_content', 'tours_page_content');
function tours_page_content()
{
    $args      = ['post_type' => 'tour', 'post_status' => 'publish','posts_per_page'=>'-1'];
    $the_query = new WP_Query($args);

?>
    <div class="block-container tours-container blockfull">
        <div class="block-inner-container">

            <div class="top-container flex-item">
                <div class="gallery-big-image slider-for">
                    <?php
                    while ($the_query->have_posts()) {
                        $the_query->the_post();
                    ?>
                        <div class="image-wrap">
                            <?php
                            if (get_field('video_url')) {
                            ?>
                                <video src="<?php the_field('video_url') ?>" controls></video>
                            <?php
                            } else {
                                the_post_thumbnail('tour_thumbnail');
                            }
                            ?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="gallery-image-info">
                    <div class="gallery-image-info-inner">
                        <span class="pre-title">סיורים לימודיים</span>
                        <div class="slider-text">
                            <h2 class='slider-title'></h2>
                            <div class="tours-date"></div>
                            <div class="tours-content"></div>
                            <?php
                            ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-container-wrap">
                <div class="bottom-container flex-item slider-nav">
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();

                    ?>
                        <div class="slider-item" data_title="<?php the_title(); ?>" data_content=" <?php the_content() ?>" data_date="<?php echo htmlspecialchars(get_field('date_text', get_the_ID())); ?>">
                            <div class="slider-image-wrap">
                                <?php the_post_thumbnail('tour_gallery'); ?>

                            </div>
                            <div class="slider-title-wrap">
                                <?php echo the_title(); ?>
                            </div>

                        </div>
                    <?php
                    }
                    wp_reset_postdata();
                    ?>
                </div>
                <div class="mobile-title-box"></div>
            </div>
        </div>
    </div>
    <div class="tours-banner blockfull">
        <div class="block-inner-container">
            <p class="pre-title">
                סיורים לימודיים
            </p>
            <p class="tours-banner-text">
                אנו מזמינים אתכם לקבוע סיור לימודי בגנזך קידוש השם
            </p>
            <a href="<?php echo get_field('visitior_info_page', 'options') ?>" class='tours-banner-button'>לתיאום סיור</a>
        </div>
    </div>
<?php
}
genesis();
