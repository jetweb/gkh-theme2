jQuery(function ($) {
	/* Sentry.init({
		dsn: 'https://6b97bc4a44774b808e71097cbd4aad74@o222553.ingest.sentry.io/5459675'
	}); */
	// Mobile Menu
	$('.menu-toggle').click(function () {
		$('.search-toggle, .header-search').removeClass('active');
		$('.menu-toggle, .nav-menu').toggleClass('active');
	});
	$('.menu-item-has-children > .submenu-expand').click(function (e) {
		$(this).toggleClass('expanded');
		e.preventDefault();
	});

	// Search toggle
	$('.search-toggle').click(function () {
		$('.menu-toggle, .nav-menu').removeClass('active');
		$('.search-toggle, .header-search').toggleClass('active');
		$('.site-header .search-field').focus();
	});

	$('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-nav',
		rtl: true,
		draggable: false
	});
	$('.slider-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		prevArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button"><</button>',
		nextArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button">></button>',
		rtl: true,
		centerMode: true,
		focusOnSelect: true,
		variableWidth: true
	});



	$('.variable-width').slick({
		dots: false,
		prevArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button"><</button>',
		nextArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button">></button>',
		infinite: true,
		speed: 300,
		rtl: true,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true
	});

	//TOURS GALLERY
	if ($('body').hasClass('page-template-template-tours')) {
		function changeTourSlides() {
			console.log($('.tours-container .slick-active')[0]['attributes']);

			title = $('.tours-container .slick-current')[1]['attributes'][1].value
			content = $('.tours-container .slick-current')[1]['attributes'][2].value
			date = $('.tours-container .slick-current')[1]['attributes'][3].value

			$('.tours-container .slider-title').html(title);
			$('.tours-container .mobile-title-box').html(title);
			$('.tours-container .tours-date').html(date);
			$('.tours-container .tours-content').html(content);
		}

		changeTourSlides();


		$('.tours-container .slider-item').on('click', changeTourSlides)
		$('.slick-arrow').on('click', changeTourSlides)

		////////////////////
	}
	//BIOGRAPHY GALLERY
	if ($('body').hasClass('page-template-template-biography')) {
		function changeMosheSlides() {
			description = $('.gallery-container .slick-active')[1]['attributes'][1].value;
			caption = $('.gallery-container .slick-active')[1]['attributes'][2].value;
			image = $('.gallery-container .slick-active')[1]['attributes'][3].value;

			$('.gallery-bottom-content-inner .gallery-image-year').html(caption);
			$('.gallery-bottom-content-inner .gallery-image-description').html(description);
			$('.gallery-big-image img').attr('src', image);
		};
		changeMosheSlides();

		$('.gallery-container .slick-slide').on('click', changeMosheSlides)
		$('.gallery-container .slick-arrow').on('click', changeMosheSlides)

		////////////////////
	}

	$('.menu-item-has-children>a').on('click', function (e) {
		e.preventDefault();
	})

	$('.videos-slider-wrap article').click(function (e) {
		e.preventDefault();
		/* 		srcArray = [];
				$.each($('.videos-slider-wrap article'), function () {
					let src = $(this).attr('video-url');
					let videoTag = "<video controls controlsList='nodownload' autoplay><source class='video-source' src='" + src + "' type='video/mp4'</video>"
					srcArray.push(videoTag);
				})
				$.fancybox.open(srcArray); */
		let src = $(this).attr('video-url');
		if(src.includes('vimeo')){
			$.fancybox.open({
				src
			})
		}else{
			html="<video controls controlsList='nodownload' autoplay><source class='video-source' src='" + src + "' type='video/mp4'</video>";
			$.fancybox.open(html);
		}
	})




	/* 	$('.single-video').on('click', function (e) {
			e.preventDefault();
			newURL = this.href;
			title = this.title;

			$('.video-source').attr('src', newURL);
			$('video').load();
			$('h3').html(title);
		})
		$('.post-summary__image').on('click', function (e) {
			e.preventDefault();
			newURL = this.href;
			title = this.title;

			$('.video-source').attr('src', newURL);
			$('video').load();
			$('h3').html(title);
		})
		$('.slick-arrow').on('click', function (e) {
			newURL = $('.slick-current').children()[0].href;
			title = $('.slick-current').children()[0].title;
			$('.video-source').attr('src', newURL);
			$('video').load();
			$('h3').html(title);
		}) */

	$(".load-more-news").on('click', function (e) {
		e.preventDefault();
		$this = this;
		$.ajax({
			url: globalData.ajaxurl,
			dataType: 'html',
			type: 'get',
			data: {
				'action': 'get_news',
				'page': this.dataset.page
			},
			success: function (res) {
				$('.articles-wrap').append(res);
				$this.dataset.page = parseInt($this.dataset.page) + 1
				if ($this.dataset.page == parseInt($this.dataset.max) + 1) {
					$($this).hide();
				}
			}
		})


	});
	$(".load-more-about-us").on('click', function (e) {
		e.preventDefault();
		$this = this;
		$.ajax({
			url: globalData.ajaxurl,
			dataType: 'html',
			type: 'get',
			data: {
				'action': 'get_about_us',
				'page': this.dataset.page
			},
			success: function (res) {
				$('.row-wrap').append(res);
				$this.dataset.page = parseInt($this.dataset.page) + 1
				if ($this.dataset.page == parseInt($this.dataset.max) + 1) {
					$($this).hide();
				}
			}
		})


	});
	$(".load-more-moshe-books").on('click', function (e) {
		e.preventDefault();

		$this = this;
		$.ajax({
			url: globalData.ajaxurl,
			dataType: 'html',
			type: 'get',
			data: {
				'action': 'get_moshe_books',
				'page': this.dataset.page
			},
			success: function (res) {
				$('.productions-grid').append(res);
				$this.dataset.page = parseInt($this.dataset.page) + 1
				if ($this.dataset.page == parseInt($this.dataset.max) + 1) {
					$($this).hide();
				}
			}
		})


	});

	$('a>.photo-image-wrap').hover(function () {
		$('.photo-item').css('overflow', 'hidden');
		this.style.marginTop = '-14px';
		this.nextElementSibling.style.color = '#FFD65D';
		this.nextElementSibling.style.marginTop = '-14px';

	})
	$('a>.photo-image-wrap').mouseleave(function () {

		this.nextElementSibling.style.color = '#fff';
		this.nextElementSibling.style.marginTop = '0';
		this.style.marginTop = '0px';
	})
	$('a>.display-image-wrap').hover(function () {
		$('.photo-item').css('overflow', 'hidden');
		this.style.marginTop = '-14px';
		this.nextElementSibling.style.color = '#FFD65D';
		this.nextElementSibling.style.marginTop = '-14px';

	})
	$('a>.display-image-wrap').mouseleave(function () {

		this.nextElementSibling.style.color = '#000';
		this.nextElementSibling.style.marginTop = '0';
		this.style.marginTop = '0px';
	})




	$('.amount-circle').on('click', function () {
		$('.amount-circle').removeClass('amount-circle-active');
		this.classList.toggle('amount-circle-active');
	})
	$('.method-bank-transfer').on('click', function () {
		if (!$('.amount-circle').hasClass('amount-circle-active')) {
			$('.error-message').show();
			return;
		} else if ($('.custom-amount').hasClass('amount-circle-active')) {
			var amount = $('.custom-amount').val();
			$('.first_field input').val(amount)
		} else {
			var amount = $('.amount-circle-active')[0].innerText;
			$('.first_field input').val(amount.substring(1, 10));
		}
		$('.donations-first-screen').hide();
		$('.donations-second-screen').show();

	})

	$('.menu-item').on('click', function () {
		$(this).find('.submenu-expand').toggleClass('expanded');
	})
	$('.submenu-expand').on('click', function () {
		$(this).toggleClass('expanded');
	})

	$('.menu-toggle').on('click', function () {

		if ($('.nav-menu').hasClass('active')) {
			$('.video-wrap').css('top', '500px')
		} else {
			$('.video-wrap').css('top', '222px')
		}
	})

	$('.pagination-next a').text('דף הבא  >>');
	$('.pagination-previous a').text('<< דף קודם');

	//disable download
	$('video').attr('controlslist', "nodownload");

	$('div').click(function () {
		setTimeout(function () {
			$('video').bind('contextmenu', function () {
				return false;
			});
		}, 200)

	})


	$('a').bind('contextmenu', function () {
		return false;
	});
	$('video').bind('contextmenu', function () {
		return false;
	});
	$('.nl-dropdown .nl-dropdown-toggle').attr('style', 'background:#f7c121!important')



	if ($('.single-exhibition').length > 0) {
		$('.book').on('click', function () {
			var bookId = $(this).data('book');
			$('#' + bookId).trigger('click')
		})

		$('.exhibition-read-more').on('click',function(){
			$('.full-content').show();
			$('.partial-content').hide();
		})
		/* if ($('.entry-content:not(.items) > p').length > 3) {
			var counter = 0;
			$('.entry-content:not(.items) > p').each(function () {
				if (counter >= 3) {
					$(this).hide();
				}
				counter++;

			})
			$('.entry-content:not(.items) > p:nth-child(3)').append('<span class="exhibition-read-more">[קרא עוד]</span>')
			$('.exhibition-read-more').on('click',function(){
				$('p').show();
				$('.exhibition-read-more').hide();
			})
		} */
	}
});