<?php
/**
 * Template Name: מידע למבקר
 *
 */

add_action('genesis_entry_content', 'display_visitor_info_page');
function display_visitor_info_page()
{
    ?>
<div class="create-visit-box blockfull">
    <div class="block-inner-container">
        <div class="visit-box-header">
            תיאום ביקור
        </div>
        <div class="flex-item">
            <div class="visit-form">
                <?php $form = get_field('gravity_form');
    gravity_form($form, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex = 20, $echo = true);?>

            </div>
            <div class="visit-info">
                <div class="visit-info-top">
                   <!--  <div class="visit-info-title">
                        כניסת מבקרים בודדים
                    </div>
                    <div class="visit-info-description">

                        הכניסה ל"גנזך קידוש השם" אינה כרוכה בתשלום.
                        מומלץ לתאם הגעה מראש
                    </div>

                    <div class="visit-info-title">
                        ביקור קבוצות
                    </div>
                    <div class="visit-info-description">
                        ביקור קבוצות מאורגנות נעשה בתיאום מועד הביקור מראש

                    </div> -->
                    <?= get_field('visitor_info','options') ?>

                </div>
                <div class="visit-info-bottom">
                    <div class="visit-info-title">
                        גנזך קידוש השם
                    </div>
                    <hr>
                    <div class="museum-info">
                    <div class="flex-item">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/location-logo.png' ?>"
                                alt="">
                            <span><?php echo get_field('ganzach_address','options') ?></span>
                        </div>
                        <div class="flex-item">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/clock-logo.png' ?>"
                                alt="">
                            <span><?php echo get_field('ganzach_hours','options') ?>
                            </span>
                        </div>
                        <div class="flex-item">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/phone-logo.png' ?>"
                                alt="">
                            <span><?php echo get_field('ganzach_phone_numbers','options') ?>
                            </span>
                        </div>
                        <div class="flex-item">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/email-logo.png' ?>"
                                alt="">
                            <span><?php echo get_field('ganzach_mail','options') ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="map-container blockfull"
        style='background-image:url(<?php echo get_stylesheet_directory_uri() . '/assets/images/map.png' ?>)'>

    </div>
    <?php
}
genesis();