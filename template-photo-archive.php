<?php
/**
 * Template Name: photo archive
 *
 */
add_action('genesis_entry_content', 'add_text');
add_action('genesis_entry_content', 'gkh_photo_archive_loop');
add_action('genesis_entry_content', 'add_exhibitions_banner');

function add_text()
{
    echo '<p class="top-paragraph">בארכיון "גנזך קידוש השם" קיימים כ- 300,000 תצלומים המתעדים נושאים שונים</p><hr>';

}
function gkh_photo_archive_loop()
{
    $args      = ['post_type' => 'visual_photos', 'post_status' => 'publish', 'order' => 'DESC', 'posts_per_page' => -1];
    $the_query = new WP_Query($args);
    ?>
<div class="photos-container">
    <div class="block-inner-container">
        <div class="photos-grid">
            <?php
while ($the_query->have_posts()) {
        $the_query->the_post();?>
            <a href='<?php echo get_permalink() ?>' class="photo-image-wrap" data-pid="<?php echo get_the_ID() ?>">
                <div class="photo-item" style='background-image:url( <?php the_post_thumbnail_url('visual_photos')?>)'>

                    <!--  <?php the_post_thumbnail('visual_photos')?> -->
                    <h2> <?php the_title()?> </h2>




                </div>
            </a>
            <?php
}?>
        </div>
    </div>
</div>
<?php
wp_reset_postdata();
}
function add_exhibitions_banner()
{
    $image_field = get_field('image');
    $text        = get_field('text');?>
<div class="block-container exhibitions-banner blockfull">
    <div class="block-inner-container flex-item">
        <div class="exhibitions-banner-text-wrap">
            <p class="pre-title">אוצרות הגנזך</p>
            <h2>תערוכות מקוונות</h2>
            <p class="exhibitions-text">
                <?php echo $text ?>
            </p>
    <?php

?>
            <a href="<?= get_field('exhibitions_page','options')->guid ?>" class="wp-block-button__link">לכל התערוכות</a>
        </div>

        <div class="exhibitions-banner-image-wrap">
            <img src="<?php echo $image_field['sizes']['tour_thumbnail']; ?>"
                alt="<?php echo $image_field['alt']; ?>" />
        </div>
    </div>
</div>

<?php
}
genesis();