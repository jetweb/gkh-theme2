<?php

/**
 * Functions
 *
 * @package      EAGenesisChild
 * @author       Bill Erickson
 * @since        1.0.0
 * @license      GPL-2.0+
 **/

/*
BEFORE MODIFYING THIS THEME:
Please read the instructions here (private repo): https://github.com/billerickson/EA-Starter/wiki
Devs, contact me if you need access
 */

/**
 * Set up the content width value based on the theme's design.
 *
 */
if (!isset($content_width)) {
    $content_width = 768;
}


/**
 * Global enqueues
 *
 * @since  1.0.0
 * @global array $wp_styles
 */
function ea_global_enqueues()
{
    // javascript
    if (!ea_is_amp()) {
        wp_register_script('ea-global', get_stylesheet_directory_uri() . '/assets/js/src/global.js', array('jquery'), filemtime(get_stylesheet_directory() . '/assets/js/src/global.js'), true);

        //wp_enqueue_script('sentry-js', '//browser.sentry-cdn.com/6.4.0/bundle.tracing.min.js', array('jquery'), CHILD_THEME_VERSION);

        wp_localize_script('ea-global', 'globalData', array('ajaxurl' => admin_url('admin-ajax.php')));
        wp_enqueue_script('ea-global');
        wp_register_script('slick', get_stylesheet_directory_uri() . '/assets/slick/slick.min.js', false, null, true);

        wp_enqueue_script('slick');
        wp_register_script('fancybox', get_stylesheet_directory_uri() . '/assets/fancybox/jquery.fancybox.min.js', false, null, true);

        wp_enqueue_script('fancybox');
    }

    // css
    wp_dequeue_style('child-theme');
    wp_register_style('ea-fonts', ea_theme_fonts_url());
    wp_register_style('ea-critical', get_stylesheet_directory_uri() . '/assets/css/critical.css', array(), filemtime(get_stylesheet_directory() . '/assets/css/critical.css'));

    wp_register_style('slick', get_stylesheet_directory_uri() . '/assets/slick/slick.css', array(), filemtime(get_stylesheet_directory() . '/assets/slick/slick.css'));

    wp_enqueue_style('slick');

    wp_register_style('fancybox', get_stylesheet_directory_uri() . '/assets/fancybox/jquery.fancybox.min.css', array(), filemtime(get_stylesheet_directory() . '/assets/fancybox/jquery.fancybox.min.css'));

    wp_enqueue_style('fancybox');

    wp_register_style('style', get_stylesheet_directory_uri() . '/style.css', array(), filemtime(get_stylesheet_directory() . '/style.css'));

    wp_enqueue_style('style');

    wp_register_style('ea-style', get_stylesheet_directory_uri() . '/assets/css/main.css', array(), filemtime(get_stylesheet_directory() . '/assets/css/main.css'));

    if ($using_critical_css = true) {
        wp_enqueue_style('ea-critical');
        wp_dequeue_style('wp-block-library');
        add_action('wp_footer', 'ea_enqueue_noncritical_css', 1);
    } else {
        ea_enqueue_noncritical_css();
    }
}
add_action('wp_enqueue_scripts', 'ea_global_enqueues');

/**
 * Enqueue Non-Critical CSS
 *
 */
function ea_enqueue_noncritical_css()
{
    wp_enqueue_style('wp-block-library');
    wp_enqueue_style('ea-critical');
    wp_enqueue_style('ea-style');
    wp_enqueue_style('ea-fonts');
}

/**
 * Gutenberg scripts and styles
 *
 */
function ea_gutenberg_scripts()
{
    wp_enqueue_style('ea-fonts', ea_theme_fonts_url());
    wp_enqueue_script('ea-editor', get_stylesheet_directory_uri() . '/assets/js/editor.js', array('wp-blocks', 'wp-dom'), filemtime(get_stylesheet_directory() . '/assets/js/editor.js'), true);
}
add_action('enqueue_block_editor_assets', 'ea_gutenberg_scripts');

add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

/**
 * Theme Fonts URL
 *
 */
function ea_theme_fonts_url()
{
    return 'https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet';
}

/**
 * Theme setup.
 *
 * Attach all of the site-wide functions to the correct hooks and filters. All
 * the functions themselves are defined below this setup function.
 *
 * @since 1.0.0
 */
function ea_child_theme_setup()
{

    define('CHILD_THEME_VERSION', filemtime(get_stylesheet_directory() . '/assets/css/main.css'));

    // General cleanup
    include_once get_stylesheet_directory() . '/inc/wordpress-cleanup.php';
    include_once get_stylesheet_directory() . '/inc/genesis-changes.php';

    include_once get_stylesheet_directory() . '/inc/admin-menu.php';

    //Taxonomies
    include_once get_stylesheet_directory() . '/inc/taxonomies/exhibitions_cat_taxonomy.php';
    include_once get_stylesheet_directory() . '/inc/taxonomies/photos_cat_taxonomy.php';
    include_once get_stylesheet_directory() . '/inc/taxonomies/videos_cat_taxonomy.php';

    //allow featured image
    add_theme_support('post-thumbnails');
    //Custom Post Types
    include_once get_stylesheet_directory() . '/inc/cpt/register_exhibitions_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_books_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_memorials_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_productions_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_studying_kits_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_tours_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_visual_testimonies_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_visual_testimonies_photos_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_display_items_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_articles_about_us_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_stories_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_moshe_books_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/register_articles_cpt.php';
    include_once get_stylesheet_directory() . '/inc/cpt/mentions.php';
    // Theme
    include_once get_stylesheet_directory() . '/inc/markup.php';
    include_once get_stylesheet_directory() . '/inc/helper-functions.php';
    include_once get_stylesheet_directory() . '/inc/layouts.php';
    include_once get_stylesheet_directory() . '/inc/navigation.php';
    include_once get_stylesheet_directory() . '/inc/loop.php';
    include_once get_stylesheet_directory() . '/inc/author-box.php';
    include_once get_stylesheet_directory() . '/inc/template-tags.php';
    include_once get_stylesheet_directory() . '/inc/site-footer.php';
    include_once get_stylesheet_directory() . '/inc/breadcrumbs.php';
    include_once get_stylesheet_directory() . '/inc/site-header.php';
    include_once get_stylesheet_directory() . '/inc/updater.php';
    include_once get_stylesheet_directory() . '/inc/pixels.php';

    // Editor
    include_once get_stylesheet_directory() . '/inc/disable-editor.php';
    include_once get_stylesheet_directory() . '/inc/tinymce.php';

    // Functionality
    include_once get_stylesheet_directory() . '/inc/login-logo.php';
    //include_once get_stylesheet_directory() . '/inc/block-area.php';
    include_once get_stylesheet_directory() . '/inc/social-links.php';
    // custom blocks
    include_once get_stylesheet_directory() . '/inc/blocks/setup-blocks.php';

    // Plugin Support
    include_once get_stylesheet_directory() . '/inc/acf.php';
    include_once get_stylesheet_directory() . '/inc/amp.php';
    include_once get_stylesheet_directory() . '/inc/shared-counts.php';
    include_once get_stylesheet_directory() . '/inc/wpforms.php';
    //include_once get_stylesheet_directory() . '/inc/critical/jetweb_critical.php';

    // Editor Styles
    add_theme_support('editor-styles');
    add_editor_style('assets/css/editor-style.css');

    // Image Sizes
    add_image_size('tour_thumbnail', 500, 400, true);
    add_image_size('news_thumbnail', 300, 274, true);
    add_image_size('biography_gallery_thumb', 9999, 200, false);
    add_image_size('visual_categories', 625, 424, true);
    add_image_size('visual_photos', 619, 431, true);
    add_image_size('studying_kits', 267, 353, true);
    add_image_size('videos_slider', 403, 276, true);
    add_image_size('productions', 301, 422, true);
    add_image_size('tour_gallery', 414, 230, true);
    add_image_size('tour_gallery', 414, 230, true);
    add_image_size('museum_header_image', 616, 443, true);
    add_image_size('single_photo_gallery', 433, 410, true);
    add_image_size('home_page_featured_article', 600, 379, true);
    // Gutenberg

    // -- Responsive embeds
    add_theme_support('responsive-embeds');

    // -- Wide Images
    add_theme_support('align-wide');

    // -- Disable custom font sizes
    add_theme_support('disable-custom-font-sizes');

    // -- Editor Font Styles
    add_theme_support('editor-font-sizes', array(
        array(
            'name'      => __('Small', 'ea_genesis_child'),
            'shortName' => __('S', 'ea_genesis_child'),
            'size'      => 14,
            'slug'      => 'small',
        ),
        array(
            'name'      => __('Normal', 'ea_genesis_child'),
            'shortName' => __('M', 'ea_genesis_child'),
            'size'      => 20,
            'slug'      => 'normal',
        ),
        array(
            'name'      => __('Large', 'ea_genesis_child'),
            'shortName' => __('L', 'ea_genesis_child'),
            'size'      => 24,
            'slug'      => 'large',
        ),
    ));

    // -- Disable Custom Colors
    add_theme_support('disable-custom-colors');

    // -- Editor Color Palette
    add_theme_support('editor-color-palette', array(
        array(
            'name'  => __('Blue', 'ea_genesis_child'),
            'slug'  => 'blue',
            'color' => '#05306F',
        ),
        array(
            'name'  => __('Grey', 'ea_genesis_child'),
            'slug'  => 'grey',
            'color' => '#FAFAFA',
        ),
    ));
}
add_action('genesis_setup', 'ea_child_theme_setup', 15);

/**
 * Change the comment area text
 *
 * @since  1.0.0
 * @param  array $args
 * @return array
 */
function ea_comment_text($args)
{
    $args['title_reply']          = __('Leave A Reply', 'ea_genesis_child');
    $args['label_submit']         = __('Post Comment', 'ea_genesis_child');
    $args['comment_notes_before'] = '';
    $args['comment_notes_after']  = '';
    return $args;
}
add_filter('comment_form_defaults', 'ea_comment_text');

/**
 * Template Hierarchy
 *
 */
function ea_template_hierarchy($template)
{
    if (is_home()) {
        $template = get_query_template('archive');
    }

    return $template;
}
add_filter('template_include', 'ea_template_hierarchy');

add_action('wp_ajax_get_news', 'ajax_get_news');
add_action('wp_ajax_nopriv_get_news', 'ajax_get_news');
add_action('wp_ajax_get_about_us', 'ajax_get_about_us');
add_action('wp_ajax_nopriv_get_about_us', 'ajax_get_about_us');
add_action('wp_ajax_get_moshe_books', 'ajax_get_moshe_books');
add_action('wp_ajax_nopriv_get_moshe_books', 'ajax_get_moshe_books');
add_action('wp_ajax_get_gallery', 'ajax_get_photo_gallery');
add_action('wp_ajax_nopriv_get_gallery', 'ajax_get_photo_gallery');

function ajax_get_photo_gallery()
{
    $post_id = $_GET['pid'];
    $gallery = get_field('gallery', $post_id);
    $items   = [];
    foreach ($gallery as $image) {
        $items[] = array('src' => $image['url']);
    }
    wp_send_json_success($items);
}
function ajax_get_news()
{

    $args      = ['post_type' => 'post', 'post_status' => 'publish', 'order' => 'DESC', 'posts_per_page' => 4, 'paged' => $_GET['page']];
    $the_query = new WP_Query($args);
?>
    <div class="articles-inner-wrap">
        <?php
        while ($the_query->have_posts()) {
            $the_query->the_post(); ?>
            <div class="news-box-article">
                <a href="<?php echo get_post_permalink() ?>" class='news-box-link'>
                    <div class="article-image">
                        <?php echo the_post_thumbnail('news_thumbnail') ?>
                    </div>
                    <div class="article-text-wrap">

                        <p class="article-title">
                            <?php the_title() ?>
                        </p>
                        <p class="read-more">לקריאה ←</p>

                    </div>
                </a>
            </div>
        <?php
        } ?>
    </div>
<?php
    wp_reset_postdata();
    die;
}
function ajax_get_about_us()
{

    $args      = ['post_type' => 'articles_about_us', 'post_status' => 'publish', 'order' => 'DESC', 'posts_per_page' => 4, 'paged' => $_GET['page']];
    $the_query = new WP_Query($args);
?>
    <div class="four-pics-image-wrap">
        <?php
        while ($the_query->have_posts()) {
            $the_query->the_post(); ?>
            <div class="image-box">
                <div class="image-wrap">
                    <a href="<?php the_post_thumbnail_url() ?>" data-fancybox>
                        <?php the_post_thumbnail(); ?>
                    </a>
                </div>

            </div>
        <?php
        } ?>
    </div>
<?php
    wp_reset_postdata();
    die;
}
function ajax_get_moshe_books()
{

    $args      = ['post_type' => 'moshebooks', 'post_status' => 'publish', 'order' => 'DESC', 'posts_per_page' => 4, 'paged' => $_GET['page']];
    $the_query = new WP_Query($args);
?>
    <?php
    $num = ((int) $_GET['page']) * 4;

    while ($the_query->have_posts()) {
        $the_query->the_post(); ?>
        <div class="production-item">
            <a data-fancybox='gallery' data-src="#hidden-content-<?php echo $num ?>" href="javascript:;">
                <div class="production-item-inner" data-src="#hidden-content">
                    <div class="production-image-wrap">
                        <?php the_post_thumbnail('productions'); ?>
                    </div>
                </div>
            </a>
            <div class="production-title"><?php the_title(); ?>
            </div>
            <p class="image-box-date">
                <?php the_field('date') ?>
            </p>

        </div>

        <div style="display: none;" class='modal' id="hidden-content-<?php echo $num ?>">
            <div class="flex-item">
                <div class="modal-image-wrap">
                    <?php the_post_thumbnail('productions'); ?>
                </div>
                <div class="modal-text-wrap">
                    <h2><?php the_title(); ?></h2>
                    <h3><?php the_field('sub_title'); ?></h3>
                    <div class="modal-content">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>

    <?php
        $num = $num + 1;
    } ?>
<?php
    wp_reset_postdata();
    die;
}
add_filter('gform_confirmation', 'my_confirmation', 10, 3);
function my_confirmation($confirmation, $form, $entry)
{

    if (stristr($form['cssClass'], 'donation') == false) {
        return $confirmation;
    }
    //If form has class .donation

    $url = 'https://kesherhk.info/RedirectPage.aspx';

    //Must
    //$data['pId']  = 16607;
    $data['pId'] = 16607; //test
    //$data['id']   = 2619;
    $data['id']   = 2619; //test
    $data['dest'] = 1;
    $data['lang'] = 'he-IL';
    $data['cur']  = 1;

    //payment
    $data['total'] = rgar($entry, '1');

    $data['unq'] = $entry['id']; // OrderID
    //$data['addData'] = $entry;

    //user

    $data['nm']      = rgar($entry, '4') . ' ' . rgar($entry, '5');
    $data['ml']      = rgar($entry, '11');
    $data['tl']      = rgar($entry, '10');
    $data['adrs']    = rgar($entry, '8') . ' ' . rgar($entry, '7');
    $data['addData'] = $entry['id'];
    //echo $data['addData'];
    $data['cType'] = rgar($entry, '13');
    if ($data['cType'] == 'תרומה חד פעמית') {
        $data['cType'] = 1;
    } else {
        $data['cType'] = 10;
    }
    $data['redirectToParent'] = false;
    //
    $data['titles']       = 'hide';
    $data['panelHeading'] = 'hide';
    $data['pciImg']       = 'hide';
    $data['buco']         = 'ffd65d';
    $data['buhoco']       = '00ff00';

    $src = $url . '?' . http_build_query($data);

    $confirmation = "<iframe scrolling='no' src='{$src}'></iframe>";

    return $confirmation;
}

function no_nopaging($query)
{
    if (is_archive()) {
        $query->set('posts_per_page', 9);
    }
}

add_action('parse_query', 'no_nopaging');
//Wrap navbar to make it fixed
add_action('genesis_before_header', function () {
?>
    <div class="navbar-wrap">


    <?php
});
add_action('genesis_after_header', function () {
    ?>
    </div>

    <script>
        document.querySelector('.site-title a').textContent = '';
    </script>

<?php
});
add_filter('wp_get_attachment_image_attributes', function ($attr, $attachment, $size) {

    $attr['title'] = $attachment->post_title;
    return $attr;
}, 10, 3);

add_action('wp_footer', 'nagishli');
function nagishli()
{
?>
    <script>
        window.PDFJS_LOCALE = {
            pdfJsWorker: '<?php echo get_stylesheet_directory_uri() ?>/inc/flipbook/js/pdf.worker.js',
            pdfJsCMapUrl: 'cmaps'
        };
        nl_pos = "bl";
        nl_color = "yellow";
        nl_compact = "0";
        nl_accordion = "1";
        nl_dir = "https://www.ganzach.org.il/wp-content/themes/GKH-Theme/inc/";
    </script>
    <script src="https://www.ganzach.org.il/wp-content/themes/GKH-Theme/inc/nagishli.js?v=2.3" charset="utf-8" defer></script>
<?php
}

remove_action('genesis_site_title', 'genesis_seo_site_title');
remove_action('genesis_before_post_content', 'genesis_post_info');



function reduce_orig_img_size()
{
    // $json_option = op_get_resize_options();
    $width      = '2048';
    $height     = '2048';
    $query_images_args = array(
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'post_status'    => 'inherit',
        'posts_per_page' => 250,
        'offset'=>0
    );
    
    $query_images = new WP_Query( $query_images_args );
    
    foreach ( $query_images->posts as $image ) {
        $path_attached = get_attached_file($image->ID);
        $path_original =  wp_get_original_image_path($image->ID);

        $img = wp_get_image_editor(/* $path_original */"/home/gkhsupre/domains/ganzach.org.il/public_html/wp-content/uploads/2023/03/מפה.jpeg");
        if(is_wp_error( $img )){
            var_dump($img);
            continue;
        }
        $size = $img->get_size();
        if ($size['width'] > $width) {
            var_dump($image->ID);
            var_dump($image->guid);
            var_dump('width: '.$size['width']);
            $img->resize($width, $height / ($size['width'] / $size['height']));
            $img->save($path_original);
            var_dump($img->get_size());
        }
    }
exit;


}
if (isset($_GET['img'])) {
    reduce_orig_img_size();
}
