<?php

/**
 * Template Name: דף: הנצחה
 *
 */
add_action('genesis_entry_content', 'memorial_page_content');
function memorial_page_content()
{
    $args      = ['post_type' => 'memorial', 'post_status' => 'publish', 'posts_per_page' => '-1'];
    $the_query = new WP_Query($args);

?>
    <div class="block-container memorial-container blockfull">
        <div class="block-inner-container">
            <div class="memorial-grid">
                <?php
                while ($the_query->have_posts()) {
                    $the_query->the_post();
                    $memorial_meta = get_post_meta(get_the_ID());
                    // print_r(get_post_meta(get_the_ID()));
                    $sub_header = $memorial_meta['sub-header'][0];
                    $credit     = $memorial_meta['credit'][0];
                    echo '<div class="memorial-item">';
                    echo '<p class="memorial-header">לזכרון עולם</p>';
                    echo "<p class='memorial-sub-header'>$sub_header</p>";
                    echo "<p class='memorial-main-text'>" . get_the_content() . " </p>";
                    echo "<p class='memorial-credit'>$credit</p>";
                    echo "<p class='memorial-footer'>ת.נ.צ.ב.ה</p>";
                    echo '</div>';
                    //the_content();

                    the_field('date_text', get_the_ID());
                }
                wp_reset_postdata();

                ?>
            </div>
        </div>
    </div>
    <?php
    $pre_title   = get_field('pre_title');
    $text        = get_field('text');
    $button_text = get_field('button_text');
    $button_url  = get_field('button_url');
    $image       = get_field('background_image');
    ?>
    <div class="memorial-banner blockfull" style='background-image:url(<?php echo $image['url'] ?>)'>
        <div class="block-inner-container">
            <p class="memorial-banner-title"><?php echo $pre_title ?></p>
            <p class="memorial-banner-text"><?php echo $text ?></p>
            <a href="<?php echo $button_url ?>" class="memorial-banner-button"><?php echo $button_text ?></a>
        </div>
    </div>


<?php
}
genesis();
