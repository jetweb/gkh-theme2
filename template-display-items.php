<?php
/**
 * Template Name: מוצגים
 *
 */

add_action('genesis_loop', 'gkh_items_loop');
add_action('genesis_after_loop', 'add_exhibitions_banner');

function gkh_items_loop()
{
    $args      = ['post_type' => 'display_item', 'post_status' => 'publish', 'order' => 'DESC', 'posts_per_page' => -1];
    $the_query = new WP_Query($args);
    ?>
<div class="photos-container display-items-container">
    <div class="block-inner-container">
        <div class="photos-grid">
            <?php
while ($the_query->have_posts()) {
        $the_query->the_post();?>
            <a href='<?php echo get_post_permalink() ?>' class="display-image-wrap">
                <div class="photo-item" style="background-image:url(<?php the_post_thumbnail_url('tour_thumbnail')?>)">
                </div>
                <h2> <?php the_title()?> </h2>
            </a>
            <?php
}?>
        </div>
    </div>
</div>
<?php
wp_reset_postdata();
}
function add_exhibitions_banner()
{
    /*   $image_field = get_field('image');
$text        = get_field('text');?>
<div class="block-container exhibitions-banner blockfull">
    <div class="block-inner-container flex-item">
        <div class="exhibitions-banner-text-wrap">
            <p class="pre-title">אוצרות הגנזך</p>
            <h2>תערוכות מקוונות</h2>
            <p class="exhibitions-text">
                <?php echo $text ?>
            </p>

            <a href="" class="wp-block-button__link">לכל התערוכות</a>
        </div>

        <div class="exhibitions-banner-image-wrap">
            <img src="<?php echo $image_field['sizes']['tour_thumbnail']; ?>"
                alt="<?php echo $image_field['alt']; ?>" />
        </div>
    </div>
</div>

<?php*/
}
genesis();