<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class jetweb_critical
{
    var $scripts;
    public function __construct()
    {
        if (isset($_GET['critical_site_map'])) {
            add_action('init', array($this, 'generate_critical_sitemap'), 999);
        }
        add_action('wp_head', array($this, 'enqueue_critical'), 10);

        /*  if (isset($_GET['critical_test'])) {
            add_action('wp_head', array($this, 'enqueue_critical'), 10);
            add_action('wp_footer', array($this, 'test_add_link'), 9999);
        } */
    }

    function add_script($name, $url, $dep)
    {
        define('CRITICAL_VERSION', '1.0.0');
        wp_enqueue_script($name, $url, $dep, CRITICAL_VERSION, true);
    }
    function localize($name, $data)
    {
        wp_localize_script('global', $name, $data);
    }

    function generate_critical_sitemap()
    {
        $critical_array = array();
        $cpts = get_post_types('', 'objects');
        unset($cpts['attachment']);
        unset($cpts['page']);
        foreach ($cpts as $key => $cpt) {
            if ($cpt->exclude_from_search || !$cpt->public) {
                unset($cpts[$key]);
            }
        }
        $pages = get_pages();
        $critical_array['page'] = array();
        foreach ($pages as $page) {
            $critical_array['page'][$page->post_name] = get_permalink($page->ID);
        }
        foreach ($cpts as $key => $cpt) {
            $cpt_posts = get_posts(array('post_type' => $key));
            if (!empty($cpt_posts)) {
                $critical_array[$key]['post'] = get_permalink($cpt_posts[0]->ID);
                $critical_array[$key]['archive'] = get_post_type_archive_link($key);
            }
        }
        update_option('critical_array', $critical_array);
        echo json_encode($critical_array, JSON_UNESCAPED_SLASHES);
        exit;
    }
    private function get_script_minify()
    {
    }
    function enqueue_critical()
    {
        echo '<div class="type-of-page" style="position: fixed; z-index: 99999; background: white; font-size: 20px;">';
        $page_type = $this->get_page_type();
        if (!empty($page_type)) {
            $include_path = get_stylesheet_directory() . '/assets/css/critical/';
            $folder = '';
            $filename = '';
            if ($page_type['type'] == 'page') {
                $folder = "pages";
                $filename = $page_type['name'] . '-' . $page_type['platform'] . '.css';
            } else {
                $folder = "cpts";
                $filename = $page_type['type'] . '-' . $page_type['name'] . '-' . $page_type['platform'] . '.css';
            }
            $include_path .= $folder . '/' . $filename;
            echo '<style class="critical">';
            include_once $include_path;
            echo '</style>';
        } else {
            echo '<style class="critical">';
            $include_path = get_stylesheet_directory() . '/assets/css/critical/general.css';
            include_once $include_path;
            echo '</style>';
        }
        echo '</div>';
    }
    function get_page_type()
    {
        $page_type = array();
        if (is_page()) {
            global $post;
            $page_type['type'] = 'page';
            $page_type['name'] = $post->post_name;
        }

        if (is_singular()) {
            $critical_array = get_option('critical_array', true);
            if(isset($_GET['kkk'])){
                var_dump($critical_array);
                exit;
            }
            unset($critical_array['page']);
            foreach ($critical_array as $post_type => $array_post_archive) {
                if (is_singular($post_type)) {
                    $page_type['type'] = 'single';
                    $page_type['name'] = $post_type;
                }
            }
        } else if (is_post_type_archive()) {
            $critical_array = get_option('critical_array', true);
            unset($critical_array['page']);
            foreach ($critical_array as $post_type => $array_post_archive) {
                if (is_post_type_archive($post_type)) {
                    $page_type['type'] = 'archive';
                    $page_type['name'] = $post_type;
                }
            }
        } else if (is_home()) {
            $page_type['type'] = 'archive';
            $page_type['name'] = 'post';
        }
        $page_type['platform'] = wp_is_mobile() ? 'mobile' : 'desktop';
        return $page_type;
    }

    function test_add_link()
    {
        echo '<script>jQuery( document ).ready(function($) {$("a").attr("href", function(i, h) {return h + (h.indexOf("?") != -1 ? "&critical_test=1" : "?critical_test=1");});});</script>';
    }
}

$jetweb_critical = new jetweb_critical();
