<?php
/**
 * Site Footer
 *
 * @package      EAGenesisChild
 * @author       Bill Erickson
 * @since        1.0.0
 * @license      GPL-2.0+
 **/

/**
 * Site Footer
 *
 */
function ea_site_footer()
{

    echo '<div class="footer-left">';
    genesis_nav_menu(array(
        'theme_location'  => 'footer',
        'container'       => 'div',
        'container_class' => 'wrap',
        'menu_class'      => 'menu genesis-nav-menu menu-footer',
        'depth'           => 1,
    ));
    echo '<span class="footer-items-wrap">&nbsp &nbsp<span class="footer-item-1"> <span class="seperator">|</span> &nbsp&nbsp @ כל הזכויות שמורות לגנזך קידוש השם </span>&nbsp&nbsp<span class="seperator">|</span>&nbsp&nbsp <span class="footer-item-2">Powered by <a href="https://www.jetweb.co.il/">Jetweb</a></span></span>';
    echo '</div>';

}
add_action('genesis_footer', 'ea_site_footer');
remove_action('genesis_footer', 'genesis_do_footer');

add_action('genesis_before_footer', 'gkh_top_footer', 8);

function gkh_top_footer()
{

    ?>
<section id='top-footer'>
    <div class="top-footer-inner">
        <div class="top-footer-items">
            <p class="top-footer-text">הוקם בסיוע</p>
            <?php
/* while (have_rows('top_footer_items', 'options')) {
    //  echo 'na';
    //var_dump(get_sub_field('image', 'options'));
    } */
    while (have_rows('top_footer_items', 'options')): the_row();
        $image_url = get_sub_field('footer_image', 'options');
        $num       = 1;
        ?>
            <img class="image<?php echo $num ?>" src="<?php echo $image_url['url'] ?>"
                alt="<?php echo $image_url['alt'] ?>">
            <?php
    $num = $num + 1;
    endwhile;
    ?>

        </div>
    </div>

</section>
<?php
}