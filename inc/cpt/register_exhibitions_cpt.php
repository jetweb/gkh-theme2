<?php
// Register Custom Post Type
function register_exhibitions_cpt()
{

    $labels = array(
        'name'                  => _x('תערוכות', 'Post Type General Name', 'gkh'),
        'singular_name'         => _x('Exhibitions', 'Post Type Singular Name', 'gkh'),
        'menu_name'             => __('תערוכות מקוונות', 'gkh'),
        'name_admin_bar'        => __('Post Type', 'gkh'),
        'archives'              => __('Item Archives', 'gkh'),
        'attributes'            => __('Item Attributes', 'gkh'),
        'parent_item_colon'     => __('Parent Item:', 'gkh'),
        'all_items'             => __('All Items', 'gkh'),
        'add_new_item'          => __('Add New Item', 'gkh'),
        'add_new'               => __('Add New', 'gkh'),
        'new_item'              => __('New Item', 'gkh'),
        'edit_item'             => __('Edit Item', 'gkh'),
        'update_item'           => __('Update Item', 'gkh'),
        'view_item'             => __('View Item', 'gkh'),
        'view_items'            => __('View Items', 'gkh'),
        'search_items'          => __('Search Item', 'gkh'),
        'not_found'             => __('Not found', 'gkh'),
        'not_found_in_trash'    => __('Not found in Trash', 'gkh'),
        'featured_image'        => __('Featured Image', 'gkh'),
        'set_featured_image'    => __('Set featured image', 'gkh'),
        'remove_featured_image' => __('Remove featured image', 'gkh'),
        'use_featured_image'    => __('Use as featured image', 'gkh'),
        'insert_into_item'      => __('Insert into item', 'gkh'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'gkh'),
        'items_list'            => __('Items list', 'gkh'),
        'items_list_navigation' => __('Items list navigation', 'gkh'),
        'filter_items_list'     => __('Filter items list', 'gkh'),
    );
    $args = array(
        'label'               => __('Post Type', 'gkh'),
        'description'         => __('Post Type Description', 'gkh'),
        'labels'              => $labels,
        'supports'            => array('title', 'editor', 'thumbnail', 'excerpt'),
        'taxonomies'          => array('exhibitions_cat'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 41,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        //'show_in_rest' => true,
    );
    register_post_type('exhibition', $args);

}
add_action('init', 'register_exhibitions_cpt', 0);