<?php

function set_admin_menu_separator() {
    global $menu;

    
    add_admin_menu_separator( 30 );
    add_admin_menu_separator( 40 );
    add_admin_menu_separator( 45 );
    add_admin_menu_separator( 50 );
}

add_action( 'admin_menu', 'set_admin_menu_separator' );



function add_admin_menu_separator( $position ) {

	global $menu;

	$menu[$position] = array(
		0	=>	'',
		1	=>	'read',
		2	=>	'separator' . $position,
		3	=>	'',
		4	=>	'wp-menu-separator'
    );
    

}

function tutsplus_separators() {
    echo '<style type="text/css">
         #adminmenu li.wp-menu-separator {margin: 0;}
         .admin-color-fresh #adminmenu li.wp-menu-separator {background: #444;}
         .admin-color-midnight #adminmenu li.wp-menu-separator {background: #4a5258;}
         .admin-color-light #adminmenu li.wp-menu-separator {background: #c2c2c2;}
         .admin-color-blue #adminmenu li.wp-menu-separator {background: #3c85a0;}
         .admin-color-coffee #adminmenu li.wp-menu-separator {background: #83766d;}
         .admin-color-ectoplasm #adminmenu li.wp-menu-separator {background: #715d8d;}
         .admin-color-ocean #adminmenu li.wp-menu-separator {background: #8ca8af;}
         .admin-color-sunrise #adminmenu li.wp-menu-separator {background: #a43d39;}
          </style>';
 }
 add_action( 'admin_head', 'tutsplus_separators' );


