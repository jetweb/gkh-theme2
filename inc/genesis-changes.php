<?php
/**
 * Genesis Changes
 *
 * @package      EAGenesisChild
 * @author       Bill Erickson
 * @since        1.0.0
 * @license      GPL-2.0+
 **/

// Theme Supports
add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption'));
add_theme_support('genesis-responsive-viewport');
add_theme_support('genesis-structural-wraps', array('header', 'entry-header', 'menu-secondary', 'site-inner', 'footer-widgets', 'footer'));

add_theme_support('genesis-footer-widgets', 2);
// Add theme support for new menu
// Add Footer Menu; Keep Primary and Secondary Menus
add_theme_support('genesis-menus', array(
    'primary' => __('Primary Navigation Menu', 'genesis'),
    'footer'  => __('Footer Navigation Menu', 'genesis'),
));
// Adds support for accessibility.
add_theme_support(
    'genesis-accessibility', array(
        '404-page',
        //    'drop-down-menu',
        'headings',
        'rems',
        'search-form',
        'skip-links',
        'screen-reader-text',
    )
);

// h3 on home
add_filter('genesis_site_title_wrap', function ($wrap) {return is_front_page() ? 'h3' : $wrap;});

// Remove admin bar styling
add_theme_support('admin-bar', array('callback' => '__return_false'));

// Don't enqueue child theme stylesheet
remove_action('genesis_meta', 'genesis_load_stylesheet');

// Remove Edit link
add_filter('genesis_edit_post_link', '__return_false');

// Remove Genesis Favicon (use site icon instead)
remove_action('wp_head', 'genesis_load_favicon');

// Remove Header Description
remove_action('genesis_site_description', 'genesis_seo_site_description');

// Remove post info and meta
remove_action('genesis_entry_header', 'genesis_post_info', 12);
remove_action('genesis_entry_footer', 'genesis_post_meta');

// Remove unused sidebars
unregister_sidebar('header-right');
unregister_sidebar('sidebar-alt');
unregister_sidebar('primary_sidebar');
/**
 * Remove Genesis Templates
 *
 */
function ea_remove_genesis_templates($page_templates)
{
    unset($page_templates['page_archive.php']);
    unset($page_templates['page_blog.php']);
    return $page_templates;
}
add_filter('theme_page_templates', 'ea_remove_genesis_templates');

/**
 * Custom search form
 *
 */
function ea_search_form()
{
    ob_start();
    get_template_part('searchform');
    return ob_get_clean();
}
add_filter('genesis_search_form', 'ea_search_form');

/**
 * Disable customizer theme settings
 *
 */
function ea_disable_customizer_theme_settings($config)
{
    $remove = ['genesis_header', 'genesis_single', 'genesis_archives', 'genesis_footer'];
    foreach ($remove as $item) {
        unset($config['genesis']['sections'][$item]);
    }
    return $config;
}
add_filter('genesis_customizer_theme_settings_config', 'ea_disable_customizer_theme_settings');

/* add_action('genesis_entry_header', 'insert_header');

function insert_header()
{

if (!is_page_template('template-future.php') && !is_front_page() && !is_page_template('template-contact-us-page.php') && !is_single()) {

if (is_page_template('template-display-items-single.php')) {
$header_image = get_field('display_header_image', 'options');
} else {
$header_image = get_field('header_image');
}
if (!$header_image) {
$header_image = get_field('default_image', 'options');
}

?>
<!-- <div class="header-background" id="header-background" style="background-image: url(<?php echo $header_image['url'] ?>)">
-->
<?php

} */
/*  if (is_page_template('template-display-items-single.php')) {

$header_image = get_field('display_header_image', 'options');

?>
<div class="header-background" id="header-background" style="background-image: url(<?php echo $header_image['url'] ?>)">

    <?php
} } */