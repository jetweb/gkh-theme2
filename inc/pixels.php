<?php

class Pixels
{


    public $UA = 'UA-171156515-1 ';

    public function __construct()
    {
        add_action('wp_head', array($this, 'insert_header_pixels'), 0);
        //   add_action('genesis_before_header', array($this, 'insert_gtm_body'));
    }

    public function insert_header_pixels()
    {

        $this->insert_analytics();
    }

    private function insert_analytics()
    {
?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $this->UA ?>"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', '<?php echo $this->UA ?>');
        </script>
    <?php
    }
  

 
}
$pixels = new Pixels(); 
