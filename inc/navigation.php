<?php

/**
 * Navigation
 *
 * @package      EAGenesisChild
 * @author       Bill Erickson
 * @since        1.0.0
 * @license      GPL-2.0+
 **/

// Don't let Genesis load menus
remove_action('genesis_after_header', 'genesis_do_nav');
remove_action('genesis_after_header', 'genesis_do_subnav');

/**
 * Mobile Menu
 *
 */
class mega_Walker extends Walker_Nav_Menu
{
    public function start_lvl(&$output, $depth = 0, $args = null)
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat($t, $depth);

        // Default class.
        $classes = array('sub-menu');

        $class_names = join(' ', apply_filters('nav_menu_submenu_css_class', $classes, $args, $depth));
        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        $output .= "{$n}{$indent}<div class='mega-menu-wrap'><ul$class_names>{$n}";
    }
    public function end_lvl(&$output, $depth = 0, $args = null)
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat($t, $depth);
        $address = get_field('ganzach_address', 'options');
        $hours = get_field('ganzach_hours', 'options');
        $phone = get_field('ganzach_phone_numbers', 'options');
        $email = get_field('ganzach_mail', 'options');
        $top_text = get_field('top_text', 'options');
        $output .= "$indent</ul><div class='contact-header'>" . '<div class="contact-details-box">
        <div class="top-text">' . $top_text . '</div>
		<div class="top-details">' . $address . ' <br>' . $hours . '</div>
		<div class="bottom-details">
			<span>טלפון: </span>' . $phone . '
			<br>
			<span>דוא"ל: </span>' . $email . '
		</div>
        <div class="buttons">
<a href=" ' . get_field('contact_us_page', 'options') . '">צור קשר</a>'
            . '<a href="' .  get_field('donations_page', 'options') . '" class="nav-button">תרומה</a></div>' . "</div>{$n}";
    }
    public function print_contact_details()
    {
?>
        <div class="contact-details-box">
            <div class="top-details">הרב מלצר 15, בני ברק, קומה 3 <br> ימים א'-ה': 11:00-17:00</div>
            <div class="bottom-details">
                <span>טלפון</span>03-5795589, 03-5703018
                <br>
                <span>דוא"ל</span> ganzach@ganzach.org.il
            </div>

        </div>
    <?php
    }
}

function ea_site_header()
{
    echo ea_mobile_menu_toggle();
    //echo ea_search_toggle();
    $megawalker = new mega_Walker();
    echo '<nav' . ea_amp_class('nav-menu', 'active', 'menuActive') . ' role="navigation">';
    if (has_nav_menu('primary')) {
        wp_nav_menu(array(
            'theme_location' => 'primary',
            'menu_id'                          => 'primary-menu',
            'container_class'                  => 'nav-primary'
            /* , 'items_wrap' => '<div><ul>%3$s</ul>sami</div>' */,
            'walker'                           => $megawalker
        ));
    }
    if (has_nav_menu('secondary')) {
        wp_nav_menu(array('theme_location' => 'secondary', 'menu_id' => 'secondary-menu', 'container_class' => 'nav-secondary'));
    }
    if (!wp_is_mobile()) {

    ?>
        <div class="navbar-images">
            <img src="<?= get_stylesheet_directory_uri() ?>/assets/images/1.webp" alt=""><img src="<?= get_stylesheet_directory_uri() ?>/assets/images/2.webp" alt=""><img src="<?= get_stylesheet_directory_uri() ?>/assets/images/3.webp" alt="">
        </div>
    <?php } ?>
    </nav>
    <?php

}
add_action('genesis_after_header', 'add_images', 11);
function add_images()
{
    if (wp_is_mobile()) {

    ?>
        <div class="navbar-images">
            <img src="<?= get_stylesheet_directory_uri() ?>/assets/images/1.webp" alt=""><img src="<?= get_stylesheet_directory_uri() ?>/assets/images/2.webp" alt=""><img src="<?= get_stylesheet_directory_uri() ?>/assets/images/3.webp" alt="">
        </div>
<?php
    }
}
add_action('genesis_header', 'ea_site_header', 11);

/**
 * Nav Extras
 *
 */
function ea_nav_extras($menu, $args)
{

    if ('primary' === $args->theme_location) {
        $menu .= '<li class="menu-item search">' . /*ea_search_toggle() */ '</li>';
    }

    return $menu;
}
add_filter('wp_nav_menu_items', 'ea_nav_extras', 10, 2);

/**
 * Search toggle
 *
 */
/*function ea_search_toggle() {
$output = '<button' . ea_amp_class( 'search-toggle', 'active', 'searchActive' ) . ea_amp_toggle( 'searchActive', array( 'menuActive', 'mobileFollow' ) ) . '>';
$output .= ea_icon( array( 'icon' => 'search', 'size' => 24, 'class' => 'open' ) );
$output .= ea_icon( array( 'icon' => 'close', 'size' => 24, 'class' => 'close' ) );
$output .= '<span class="screen-reader-text">Search</span>';
$output .= '</button>';
return $output;
}*/

/**
 * Mobile menu toggle
 *
 */
function ea_mobile_menu_toggle()
{
    $output = '<button' . ea_amp_class('menu-toggle', 'active', 'menuActive') . ea_amp_toggle('menuActive', array('searchActive', 'mobileFollow')) . '>';
    $output .= ea_icon(array('icon' => 'menu', 'size' => 24, 'class' => 'open'));
    $output .= ea_icon(array('icon' => 'close', 'size' => 24, 'class' => 'close'));
    $output .= '<span class="screen-reader-text">Menu</span>';
    $output .= '</button>';
    return $output;
}

/**
 * Add a dropdown icon to top-level menu items.
 *
 * @param string $output Nav menu item start element.
 * @param object $item   Nav menu item.
 * @param int    $depth  Depth.
 * @param object $args   Nav menu args.
 * @return string Nav menu item start element.
 * Add a dropdown icon to top-level menu items
 */
function ea_nav_add_dropdown_icons($output, $item, $depth, $args)
{

    if (!isset($args->theme_location) || 'primary' !== $args->theme_location) {
        return $output;
    }

    if (in_array('menu-item-has-children', $item->classes, true)) {

        // Add SVG icon to parent items.
        $icon = ea_icon(array('icon' => 'navigate-down', 'size' => 8, 'title' => 'Submenu Dropdown'));

        $output .= sprintf(
            '<button' . ea_amp_nav_dropdown($args->theme_location, $depth) . ' tabindex="-1">%s</button>',
            $icon
        );
    }

    return $output;
}
add_filter('walker_nav_menu_start_el', 'ea_nav_add_dropdown_icons', 10, 4);
