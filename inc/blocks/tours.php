<?php
global $post;

$args      = ['post_type' => 'tour', 'post_status' => 'publish'];
$the_query = new WP_Query($args);

/* $first_post = get_posts(array(
'post_type'   => 'tour',
'numberposts' => 1,
));
$first_post_date = get_post_meta($first_post[0]->ID)['date_text'][0]; */

?>
<div class="block-container tours-container blockfull">
    <div class="block-inner-container">

        <div class="top-container flex-item">
            <div class="gallery-big-image slider-for">
                <?php
while ($the_query->have_posts()) {
    $the_query->the_post();
    the_post_thumbnail('tour_thumbnail');
}
?>
            </div>
            <div class="gallery-image-info">
                <div class="gallery-image-info-inner">
                    <span class="pre-title">סיורים לימודיים</span>
                    <div class=" slider-for">
                        <?php while ($the_query->have_posts()) {
    $the_query->the_post();
    echo '<div class="text-wrap">';
    echo "<h2>" . get_the_title() . "</h2>";
    echo '<p class="tours-date">';
    the_field('date_text', get_the_ID());
    echo '</p>';

    echo '<div class="tours-content">' . get_the_content() . '</div>';

    echo '</div>';
}?>

                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-container flex-item slider-nav">
            <?php while ($the_query->have_posts()) {
    $the_query->the_post();
    ?>
            <div class="slider-item">
                <div class="slider-image-wrap">
                    <?php the_post_thumbnail('tour_gallery');?>
                </div>
                <div class="slider-title-wrap">
                    <?php echo the_title(); ?>
                </div>

            </div>
            <?php
}
wp_reset_postdata();
?>
        </div>
    </div>
</div>