<?php
$image      = get_field('image');
$background = get_field('background');
$text       = get_field('text');
$title      = get_field('title');
$url        = get_field('future_museum_page', 'options');
?>
<div class="block-container home-future-museum blockfull"
    style="background-image:url('<?php echo $background['url'] ?>')">
    <div class="block-inner-container flex-item">
        <div class="top-div-image">
            <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
        </div>
        <div class="top-div-text text-wrap">
            <h2 class="top-div-title"><?php echo $title ?></h2>
            <p class="top-div-description"><?php echo $text ?></p>
            <a href="<?php echo $url ?>" class="read-more">המשך קריאה</a>
        </div>
    </div>
</div>