<?php
global $post;
$all_test  = get_field('visual_test_page', 'options');
$give_test = get_field('contact_us_page', 'options');
$args      = ['post_type' => 'visual_testimony', 'post_status' => 'publish', 'order' => 'DESC', 'posts_per_page' => 4];
$the_query = new WP_Query($args);
?>
<div class="block-container blockfull home-testimonies-container ">
    <div class="home-testimonies-inner">
        <h2 class="home-testimonies-title">
            עדויות ויזואליות
        </h2>

        <div class="home-testimonies-text">

            <p class="home-testimonies-inner-text">גנזך קידוש השם בתמיכת מינהל התרבות, ועידת התביעות, קרן העזבונות
                והחברה להשבת רכוש עוסק באופן אינטנסיבי בגביית עדויות מניצולי שואה על ידי צוות תחקירנים שהוכשר לכך</p>
            <div class="home-testimonies-buttons">
                <a href="<?php echo $give_test ?>">למסירת עדות</a>
                <a href="<?php echo $all_test ?>">לכל העדויות</a>
            </div>
        </div>
        <div class="home-testimonies-images" id='home-testimonies-images'>
            <?php
while ($the_query->have_posts()) {
    $the_query->the_post();
    $video_url = get_post_meta(get_the_id())['video'][0];

    ?>
            <div class="home-testimony-item" style='background-image:url( <?php the_post_thumbnail_url();?>)'>
                <a data-fancybox href="<?php echo $video_url ?>">
                    <div class="testimony-image-wrap">



                    </div>
                </a>
                <div class="testimony-text-wrap">
                    <div class="testimony-from">
                        עדות מאת
                    </div>
                    <div class="testimonies-title">
                        <a data-fancybox href="<?php echo $video_url ?>">
                            <?php the_title();?>
                        </a>
                    </div>
                </div>
            </div>
            <?php
}
wp_reset_postdata();

?>
        </div>
    </div>
</div>