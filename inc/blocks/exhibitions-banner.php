<?php
//echo '<pre>';
$image_field = get_field('image');
$text        = get_field('text');
//$image       = wp_get_attachment_image($image_field, 'medium');
//var_dump($image_field);
?>

<div class="block-container exhibitions-banner blockfull">
    <div class="block-inner-container flex-item">
        <div class="exhibitions-banner-text-wrap">
            <p class="pre-title">אוצרות הגנזך</p>
            <h2>תערוכות מקוונות</h2>
            <p class="exhibitions-text">
                <?php echo $text ?>
            </p>
            <a href="<?= get_field('exhibitions_page','options') ?>" class="wp-block-button__link">לכל התערוכות</a>
        </div>

        <div class="exhibitions-banner-image-wrap">
            <img src="<?php echo $image_field['sizes']['tour_thumbnail']; ?>"
                alt="<?php echo $image_field['alt']; ?>" />
        </div>
    </div>
</div>