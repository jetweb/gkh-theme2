<?php
$image = get_field('image');
$text  = get_field('text');
$title = get_field('title');
$url   = get_field('biography_page', 'options');

?>

<div class="block-container home-moshe blockfull">
    <div class="block-inner-container flex-item">
        <div class="bottom-div-text text-wrap">
            <p class="bottom-div-pre-title pre-title">מייסד הגנזך</p>
            <h2 class="bottom-div-title"><?php echo $title ?></h2>
            <p class="bottom-div-description"><?php echo $text ?></p>
            <a href="<?php echo $url ?>" class="read-more">המשך קריאה</a>

        </div>
        <div class="bottom-div-image">
            <img src="<?php echo $image['url'] ?>" alt="">
        </div>
    </div>
</div>