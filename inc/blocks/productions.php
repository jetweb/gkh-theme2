<?php
global $post;
$args      = ['post_type' => 'production', 'post_status' => 'publish'];
$the_query = new WP_Query($args);
?>
<div class="block-container productions-container">
    <div class="productions-grid">
        <?php
while ($the_query->have_posts()) {
    $the_query->the_post();
    echo '<div class="production-item">';
    the_post_thumbnail('tour_thumbnail');
    the_title();
    echo '<div>';
    // the_content();

    // the_field('date_text', get_the_ID());
}
wp_reset_postdata();

?>
    </div>
</div>