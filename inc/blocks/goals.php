<?php
/**
 * Block Name: Testimonial
 *
 * This is the template that displays the testimonial block.
 */

// get image field (array)
$image_1             = get_field('image_1');
$image_1_title       = get_field('image_1_title');
$image_1_description = get_field('image_1_description');
$image_2             = get_field('image_2');
$image_2_title       = get_field('image_2_title');
$image_2_description = get_field('image_2_description');
$image_3             = get_field('image_3');
$image_3_title       = get_field('image_3_title');
$image_3_description = get_field('image_3_description');
$image_4             = get_field('image_4');
$image_4_title       = get_field('image_4_title');
$image_4_description = get_field('image_4_description');
$title               = get_field('title');
$description         = get_field('description');

// create id attribute for specific styling
$id = 'goals-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<div class="block-container goals-container blockfull" id=<?=$id?>>
    <div class="block-inner-container goals-inner">
        <h2 class="block-goals-title">
            <?php echo $title ?>
        </h2>
        <p class="block-goals-description">
            <?php echo $description ?>
        </p>
        <div class="goals-image-wrap flex-item" id='goals-image-wrap'>
            <div class="image-section">

                <div class="section-image-wrap">
                    <img src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['alt']; ?>" />
                </div>
                <div class="section-text-wrap">
                    <p class="image-section-title">
                        <?php echo $image_1_title ?>
                    </p>
                    <p class="image-section-description">
                        <?php echo $image_1_description ?>
                    </p>
                </div>

            </div>
            <div class="image-section">

                <div class="section-image-wrap">
                    <img src="<?php echo $image_2['url']; ?>" alt="<?php echo $image_2['alt']; ?>" />
                </div>
                <div class="section-text-wrap">
                    <p class="image-section-title">
                        <?php echo $image_2_title ?>
                    </p>
                    <p class="image-section-description">
                        <?php echo $image_2_description ?>
                    </p>
                </div>

            </div>
            <div class="image-section">

                <div class="section-image-wrap">
                    <img src="<?php echo $image_3['url']; ?>" alt="<?php echo $image_3['alt']; ?>" />
                </div>
                <div class="section-text-wrap">
                    <p class="image-section-title">
                        <?php echo $image_3_title ?>
                    </p>
                    <p class="image-section-description">
                        <?php echo $image_3_description ?>
                    </p>
                </div>

            </div>
            <div class="image-section">

                <div class="section-image-wrap">
                    <img src="<?php echo $image_4['url']; ?>" alt="<?php echo $image_4['alt']; ?>" />
                </div>
                <div class="section-text-wrap">
                    <p class="image-section-title">
                        <?php echo $image_4_title ?>
                    </p>
                    <p class="image-section-description">
                        <?php echo $image_4_description ?>
                    </p>
                </div>

            </div>
        </div>
    </div>
</div>