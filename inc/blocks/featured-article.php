<?php
//echo '<pre>';

$post_info        = get_field('article');
$type             = $post_info->post_type;
$exhibitions_page = get_field('exhibitions_page', 'options');
$news_page        = get_field('news_page', 'options');

$excerpt = $post_info->post_excerpt;
if ($type == 'exhibition') {
    $link         = get_permalink($exhibitions_page);
    $flip         = 'flip';
    $pre_title    = 'תערוכות מקוונות';
    $white_button = 'לכל התערוכות';
} else if ($type == 'post') {
    $link         = get_permalink($news_page);
    $flip         = '';
    $pre_title    = 'חדשות הגנזך';
    $white_button = 'לכל החדשות';
} else if ($type == 'article') {
    $link         = get_field('articles_page', 'options');
    $flip         = '';
    $pre_title    = 'מאמרי הגנזך';
    $white_button = 'לכל המאמרים';
} else if ($type == 'mentions') {
    $link         = get_field('mentions_page', 'options');
    $flip         = '';
    $pre_title    = 'ב"גנזך קידוש השם" מציינים את...';
    $white_button = 'לכל האזכורים';
}

$image = get_the_post_thumbnail($post_info->ID,'home_page_featured_article');

?>

<div class="block-container home-article-container blockfull" id="atricle-block-<?php echo $post_info->ID ?>">
    <div class="block-inner-container home-article-inner flex-item <?php echo $flip ?>" id='home-article-inner'>
        <div class="article-text-wrap text-wrap">
            <p class="pre-title"><?php echo $pre_title ?></p>
            <h2><?php echo $post_info->post_title ?></h2>
            <p class="article-summary"><?php echo $excerpt ?></p>
            <div class="button-container">
                <a href="<?php echo get_permalink($post_info) ?>">קרא עוד</a>
                <a href="<?php echo $link ?>"><?php echo $white_button ?></a>
            </div>
        </div>
        <div class="article-image-wrap">
            <?php echo $image ?>
        </div>
    </div>
</div>