<?php
//echo '<pre>';
global $post;
/* $args = ['post_type' => 'visual_testimony', 'post_status' => 'publish'];
$the_query = new WP_Query($args);
 */

$terms = get_terms('photos_cat', array(
    'hide_empty' => false,
));
//print_r($terms);
?>


<div class="block-container visual-photos-container blockfull">
    <div class="block-inner-container">
        <div class="categories-grid">
            <?php

foreach ($terms as $term) {
    echo '<div class="visual-photos-category">';
    $images = get_field('image', $term);?>

            <img src="<?php echo $images['sizes']['visual_categories'] ?>" class="<?php echo $term->slug . '-image' ?>"
                alt="<?php echo $images['alt'] ?>" />
            <div class="visual-photos-text-wrap">
                <h2><?php echo $term->name; ?></h2>
            </div>

        </div>

        <?php
}
wp_reset_postdata();
?>
    </div>
</div>
</div>