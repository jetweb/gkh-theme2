<?php

$title  = get_field('title');
$text   = get_field('text');
$button = get_field('button');
$url    = get_field('button_url');
$image  = get_field('image');

?>

<div class="contact-us-container blockfull">
    <div class="block-inner-container flex-item" style="background-image:url(<?php echo $image['url'] ?>)">
        <div class="contact-us-form">
            <div class="contact-us-title">צרו קשר</div>
            <div class="contact-us-sub-title">עזרו למוסד להמשיך במפעליו החשובים להנצחת השואה</div>
            <?php $form = get_field('gravity_form');
gravity_form($form, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex = 20, $echo = true);?>
        </div>
        <div class="contact-us-text">
            <div class="contact-us-text-border">

                <div class="contact-us-text-inner">

                    <p class="right-box-title"><?php echo $title ?></p>
                    <p class="right-box-description"><?php echo $text ?></p>
                    <a href="<?php echo $url ?>" class="read-more-button"><?php echo $button ?></a>
                </div>
            </div>


        </div>
    </div>
</div>