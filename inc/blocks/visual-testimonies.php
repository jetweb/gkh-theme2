<?php
//echo '<pre>';

$terms = get_terms('videos_cat', array(
    'hide_empty' => false,
));
//print_r($_SERVER);

?>

<div class="block-container visual-testimonies-container blockfull">

    <div class="block-inner-container">
        <div class="categories-grid">
            <?php

foreach ($terms as $term) {
    $images = get_field('image', $term);
    $amount = get_term_meta($term->term_id)["amount"][0];
    ?><a href="<?php echo get_term_link($term) ?>">
                <div class='visual-testimonies-category'
                    style='background-image:url(<?php echo $images['sizes']['visual_categories'] ?>)'>

                    <div class="visual-text-wrap">
                        <h2><?php echo $term->name ?></h2>
                        <span class="visual-amount">
                            <span class="number">
                                <?php echo $amount ?>
                            </span>
                            עדויות במאגר</span>
                    </div>

                </div>
            </a>
            <?php
}

wp_reset_postdata();
?>
        </div>
    </div>
</div>