<?php
$main_title = get_field('main_title');
?>
<div class="external-sites-container blockfull">
    <div class="block-inner-container">
        <h3><?php echo $main_title ?></h3>
        <div class="bottom-line"></div>
        <?php
if (have_rows('site_links')):
?>
        <div class="sites-grid">
            <?php
while (have_rows('site_links')): the_row();
    $image      = get_sub_field('image');
    $site_title = get_sub_field('site_title');
    $url        = get_sub_field('url');
    ?>
            <a href='<?php echo $url ?>' target="_blank" class="site-wrap">
                <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
                <div class="site-title"><?php echo $site_title ?></div>
            </a>

            <?php

endwhile;?>
        </div>
        <?php
endif;
?>
    </div>
</div>