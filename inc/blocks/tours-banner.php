<div class="tours-banner blockfull">
    <div class="block-inner-container">
        <p class="pre-title">
            סיורים לימודיים
        </p>
        <p class="tours-banner-text">
            אנו מזמינים אתכם לקבוע סיור לימודי בגנזך קידוש השם
        </p>
        <a href="" class='tours-banner-button'>לתיאום סיור</a>
    </div>
</div>