<?php

$blocks = [];

// register a testimonial block
$blocks[] = array(
    'name'            => 'testimonial',
    'title'           => __('Testimonial'),
    'description'     => __('A custom testimonial block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('testimonial', 'quote'),
);
// register a goals block
$blocks[] = array(
    'name'            => 'goals',
    'title'           => __('Goals'),
    'description'     => __('A custom goals block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('goals', 'quote'),
);
// register a four_pics block
$blocks[] = array(
    'name'            => 'four_pics',
    'title'           => __('4 תמונות בשורה'),
    'description'     => __('A custom four pics block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('four_pics', 'quote'),
);
// register a memorial block
/* $blocks[] = array(
'name'            => 'memorial',
'title'           => __('Memorial'),
'description'     => __('A custom memorial block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('memorial', 'quote'),
); */
// register a exhibitions block
/* $blocks[] = array(
'name'            => 'exhibitions',
'title'           => __('exhibitions'),
'description'     => __('A custom exhibitions block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('exhibitions', 'quote'),
); */
// register a memorial banner block
/* $blocks[] = array(
'name'            => 'memorial_banner',
'title'           => __('memorial banner'),
'description'     => __('A custom memorial banner block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('memorial_banner', 'quote'),
); */
// register a museum sections block
/* $blocks[] = array(
'name'            => 'museum_sections',
'title'           => __('museum sections'),
'description'     => __('A custom museum sections block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('museum_sections', 'quote'),
); */
// register a home news article block
$blocks[] = array(

    'name'            => 'featured_article',
    'title'           => 'הצגת תוכן',
    'description'     => __('A custom featured article block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('featured_article', 'quote'),
);
// register a news page small article block
$blocks[] = array(
    'name'            => 'news_small_article',
    'title'           => __('בלוק חדשות'),
    'description'     => __('A custom news small article block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('news_small_article', 'quote'),
);
// register a gallery block
$blocks[] = array(
    'name'            => 'gallery',
    'title'           => __('גלריה- משה פראגר'),
    'description'     => __('A custom gallery block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('gallery', 'quote'),
);
// register a visual testimonies block
/* $blocks[] = array(
'name'            => 'visual_testimonies',
'title'           => __('visual testimonies'),
'description'     => __('A custom visual testimonies block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('visual_testimonies', 'quote'),
); */
// register a tours block
/* $blocks[] = array(
'name'            => 'tours',
'title'           => __('tours'),
'description'     => __('A custom tours block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('tours', 'quote'),
); */
// register a books block
/* $blocks[] = array(
'name'            => 'books',
'title'           => __('books'),
'description'     => __('A custom books block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('books', 'quote'),
);
// register a productions block
$blocks[] = array(
'name'            => 'productions',
'title'           => __('productions'),
'description'     => __('A custom productions block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('productions', 'quote'),
); */
// register a studying kit block
$blocks[] = array(
    'name'            => 'studying_kits',
    'title'           => __('ערכות לימוד'),
    'description'     => __('A custom studying_kits block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('studying_kits', 'quote'),
);
// register a contact us block
$blocks[] = array(
    'name'            => 'contact_us',
    'title'           => __('בלוק- צור קשר'),
    'description'     => __('A custom contact us block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('contact_us', 'quote'),
);

// register a visual testimonies photos block
/* $blocks[] = array(
'name'            => 'visual_testimonies_photos',
'title'           => __('visual testimonies photos'),
'description'     => __('A custom visual testimonies photos block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('visual_testimonies_photos', 'quote'),
);
// register a videos inner block
$blocks[] = array(
'name'            => 'videos_inner',
'title'           => __('videos inner'),
'description'     => __('A custom videos inner block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('videos_inner', 'quote'),
); */
// register a home visual testimonies block
$blocks[] = array(
    'name'            => 'home_visual_testimonies',
    'title'           => __('הצגת עדויות ויזואליות'),
    'description'     => __('A custom home visual testimonies block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('home_visual_testimonies', 'quote'),
);
// register a search banner block
/* $blocks[] = array(
'name'            => 'search_banner',
'title'           => __('search banner'),
'description'     => __('A custom search banner block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('search_banner', 'quote'),
); */
// register a home exhibition block
/* $blocks[] = array(
'name'            => 'home_exhibition',
'title'           => __('home exhibition'),
'description'     => __('A custom home exhibition block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('home_exhibition', 'quote'),
); */

$blocks[] = array(
    'name'            => 'home_museum',
    'title'           => ('דף הבית- מוזיאון עתידי'),
    'description'     => __('A custom home museum block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('home_museum', 'quote'),
);

$blocks[] = array(
    'name'            => 'home_moshe',
    'title'           => __('דף הבית- משה פראגר'),
    'description'     => __('A custom home moshe block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('home_moshe', 'quote'),
);
$blocks[] = array(
    'name'            => 'moshe_books',
    'title'           => __('ספרי משה פראגר'),
    'description'     => __('A custom home moshe block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('moshe_books', 'quote'),
);
// register a tours_banner block
/* $blocks[] = array(
'name'            => 'tours_banner',
'title'           => __('Tours Banner'),
'description'     => __('A custom  tours_banner block.'),
'render_callback' => 'gkh_block_render',
'category'        => 'formatting',
'icon'            => 'admin-comments',
'keywords'        => array('tours_banner', 'quote'),
); */
// register a biography block

$blocks[] = array(
    'name'            => 'biography',
    'title'           => __('ביוגרפיה'),
    'description'     => __('A custom  biography block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('biography', 'quote'),
);
// register a exhibitions banner block

$blocks[] = array(
    'name'            => 'exhibitions_banner',
    'title'           => __('באנר תערוכות'),
    'description'     => __('A custom  exhibitions banner block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('exhibitions_banner', 'quote'),
);
$blocks[] = array(
    'name'            => 'articles_about_us',
    'title'           => __('כתבו עלינו'),
    'description'     => __('A custom  exhibitions banner block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('articles_about_us', 'quote'),
);
$blocks[] = array(
    'name'            => 'display_items_banner',
    'title'           => __('מסירת מוצגים'),
    'description'     => __('A custom  exhibitions banner block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('display_items_banner', 'quote'),
);
$blocks[] = array(
    'name'            => 'external_sites',
    'title'           => __('קישורים נוספים'),
    'description'     => __('A custom  exhibitions banner block.'),
    'render_callback' => 'gkh_block_render',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('external_sites', 'quote'),
);