<?php
$year = get_field('year');
$text = get_field('text');
?>
<div class="biography-block blockfull">
    <div class="block-inner-container flex-item">
        <p class="biography-text"><?php echo $text ?></p>
        <span class="biography-year"><?php echo $year ?></span>
    </div>
</div>