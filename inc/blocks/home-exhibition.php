<?php
$post_info = get_field('exhibition');
//var_dump($post_info);
$post_meta = get_post_meta($post_info->ID);
//var_dump($post_meta);
$image = wp_get_attachment_image($post_meta['image'][0], 'large');
?>

<div class="block-container home-exhibition-container blockfull">
    <div class="block-inner-container home-exhibition-inner flex-item">
        <div class="exhibition-image-wrap">
            <?php echo $image ?>
        </div>
        <div class="exhibition-text-wrap text-wrap">
            <h4 class=" pre-title">תערוכות מקוונות</h4>
            <h2><?php echo $post_info->post_title ?></h2>
            <p class="exhibition-article-summary"><?php echo $post_meta['summary'][0] ?></p>
            <div class="button-container">
                <a href="">לצפייה בתערוכה</a>
                <a href="">לכל התערוכות</a>
            </div>
        </div>
    </div>

</div>