<div class="search-banner blockfull">
    <div class="block-inner-container">

        <h2>
            חיפוש עדות
        </h2>
        <p class="search-banner-content">
            "גנזך קידוש השם" עוסק באופן אינטנסיבי בגביית עדויות מניצולי שואה על ידי צוות תחקירנים שהוכשר לכך. <br>עד
            היום
            נגבו
            מאות רבות של עדויות המספרות את סיפור האמונה וניצחונה של הרוח.
        </p>
        <div class="search-banner-buttons">
            <a href="" class=''>לחיפוש במאגר מורחב</a>
            <a href="" class=''>תיאום ראיון עדות</a>
        </div>
    </div>
</div>