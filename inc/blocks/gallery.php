<?php
//echo '<pre>';
$images = get_field('gallery');
//var_dump(get_field('gallery'));
//print_r($images);

?>

<div class="block-container gallery-container blockfull">
    <div class="block-inner-container">
        <div class="top-container flex-item">
            <div class="gallery-big-image slider-for">
                 <?php foreach ($images as $img) {?>
                <img src='<?php echo $img["sizes"]["tour_thumbnail"] ?>' alt='<?php echo $img["alt"] ?> ' />
                <?php
}
?> 
 <img src='' alt='' />
            </div>
            <div class="gallery-image-info">
                <div class="gallery-top-content">
                    <span class="pre-title">ר' משה פראגר</span>

                    <h2>אלבום תמונות</h2>
                </div>
                <div class="gallery-bottom-content">
                    <div class="gallery-bottom-content-inner">
                        <span class="gallery-image-year">
                        </span>
                        <p class="gallery-image-description">
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-container flex-item slider-nav" id="bottom-container">
            <?php foreach ($images as $img) {?>
            <div class="bottom-container-inner" data-description="<?php echo $img['description'] ?>"
            data-caption="<?php echo $img['caption'] ?>"
            data-image="<?php echo $img["sizes"]["tour_thumbnail"] ?>">
                <img src='<?php echo $img["sizes"]["biography_gallery_thumb"] ?>' alt='<?php echo $img["alt"] ?> ' />
            </div>
            <?php
}
?>
        </div>
    </div>
</div>