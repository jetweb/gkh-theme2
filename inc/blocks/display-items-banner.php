<div class="display-items-banner blockfull">
    <div class="block-inner-container">

        <h2>
            מסירת מוצגים למוזאון גנזך קידוש השם </h2>
        <p class="display-items-content">
            אנו מזמינים אתכם למסור מוצגים ארכיונים אשר יוצגו במוזיאון הגנזך, בתרומה לגנזך תאפשרו למוסד <br>להמשיך
            במפעליו החשובים ולעמוד ביעדיו ההיסטוריים, החינוכיים והחברתיים
        </p>
        <div class="display-items-buttons">
            <a href="">מסירת מוצגים לארכיןן הגנזך</a>
        </div>
    </div>
</div>