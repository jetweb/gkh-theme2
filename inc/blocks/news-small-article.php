<?php
$args      = ['post_type' => 'post', 'post_status' => 'publish', 'order' => 'DESC', 'posts_per_page' => 4];
$the_query = new WP_Query($args);

?>

<div class="block-container news-small-article-container blockfull">
    <div class="block-inner-container news-inner-container">
        <h2>חדשות הגנזך</h2>
        <p class="news-box-sub-title">חדשות ועדכונים אחרונים בגנזך קידוש השם</p>
        <div class="articles-wrap">
            <div class="articles-inner-wrap">
                <?php
while ($the_query->have_posts()) {
    $the_query->the_post();
    ?>
                <div class="news-box-article">
                    <a href="<?php echo get_post_permalink() ?>" class='news-box-link'>
                        <div class="article-image">
                            <?php echo the_post_thumbnail('news_thumbnail') ?>
                        </div>
                        <div class="article-text-wrap">

                            <p class="article-title">
                                <?php the_title()?>
                            </p>
                            <p class="read-more">לקריאה ←</p>

                        </div>
                    </a>
                </div>
                <?php
}
;?>


            </div>
        </div>
        <?php
if ($the_query->max_num_pages > 1) {
    ?>
        <div class="button-wrap">
            <a href='#' data-page='2' data-max="<?php echo $the_query->max_num_pages ?>" class="load-more-news">
                טען עוד
            </a>
        </div>
        <?php
}
wp_reset_postdata()?>

    </div>
</div>