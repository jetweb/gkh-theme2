<?php
global $post;

$args      = ['post_type' => 'studying_kit','posts_per_page' => '-1', 'post_status' => 'publish'];
$the_query = new WP_Query($args);
?>
<div class="block-container studying-kits-container blockfull">
    <div class="block-inner-container">
        <!-- <p class="studying-kits-block-title">ערכות לימוד</p>
<p class="studying-kits-block-description">גנזך קידוש השם הפיק סדרה של חוברות לימוד, המבוססים על התחקירים והאוצרות שברשותו. חוברות הלימוד מאפשרות לציבור המבקרים, כמו-גם לכל הגולשים ברשת, גישה למקורות הארכיונים ,למטרות של הוראה, ולפעילות אינטראקטיבית.

</p> -->
        <div class="studying-kits-grid">
            <?php
            $num=1;
while ($the_query->have_posts()) {
    $the_query->the_post();
    //var_dump(get_post_meta(get_the_ID())['_file'][0]);
    echo '<div class="studying-kits-item flex-item">';
    echo '<div class="studying-kits-item-text">';
    echo '<div class="studying-kits-item-text-inner">';
    echo '<p class="studying-kit-title">';
    the_title();
    echo '</p>';
    echo get_the_content();

    echo '<p class="studying-kit-includes">הערכה כוללת</p>';
    echo '<ul class="studying-kit-features">';
    echo '<li class="studying-kit-features-item">';
    the_field('feature_1', get_the_ID());
    echo '</li>';
    echo '<li class="studying-kit-features-item">';
    the_field('feature_2', get_the_ID());

    echo '</li>';
    echo '<li class="studying-kit-features-item">';
    the_field('feature_3', get_the_ID());
    echo '</li>';
    echo '</ul>';
    echo '</div>';
    if(get_field('read_more', get_the_ID())){
    ?>
     <a data-fancybox data-src="#hidden-content-<?php echo $num ?>" href="javascript:;">
    <div class="studying-kit-read-more">
                <div class="studying-kit-read-more-inner">
                    <div class="studying-kit-download-text">
                      קרא עוד
                    </div>
                </div>
            </div>
        </a>

          <!--   <div class="production-item">
                <a data-fancybox data-src="#hidden-content-<?php echo $num ?>" href="javascript:;">
                    <div class="production-item-inner" data-src="#hidden-content">
                        <div class="production-image-wrap">
                            <?php the_post_thumbnail('productions');?>
                        </div>
                    </div>
                </a>
                <div class="production-title"><?php the_title();?>
                </div>


            </div>
 -->
            <div style="display: none; text-align: right; direction: rtl;" class='modal' id="hidden-content-<?php echo $num ?>">
                    <div class="modal-text-wrap">
                        <h2><?php the_title();?></h2>
                        <div class="modal-content">
                            <?php the_content();?>
                        </div>
                    </div>
            </div>





            <?php

    echo '</a>'; 
    }
   
    echo '</div>';
    echo '<div class="studying-kits-item-image">';
    the_post_thumbnail('studying_kits');

    echo '</div>';
    echo '</div>';
    // the_content();

    // the_field('date_text', get_the_ID());
    $num++;
}
wp_reset_postdata();

?>

        </div>
    </div>
</div>