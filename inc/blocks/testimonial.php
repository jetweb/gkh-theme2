<?php
/**
 * Block Name: Testimonial
 *
 * This is the template that displays the testimonial block.
 */

// get image field (array)
$image = get_field('image');
$pre_header = get_field('pre_header');
$header = get_field('header');
$text = get_field('text');

// create id attribute for specific styling
$id = 'testimonial-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<div class="block-container testimonial-container" id=<?=$id?>>

<div class="image-wrap">
<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
</div>

<div class="text-wrap">

<small class="pre-header">
<?php echo $pre_header ?>
</small>
<h4 class="block-header">
<?php echo $header ?>
</h4>
<p class="block-text">
<?php echo $text ?>
</p>
</div>
</div>
