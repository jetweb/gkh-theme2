<?php
//echo '<pre>';
$args      = ['post_type' => 'articles_about_us', 'post_status' => 'publish', 'order' => 'ASC', 'posts_per_page' => 4];
$the_query = new WP_Query($args);
//var_dump($post_info);

?>

<div class="block-container articles-about-us-container four-pics-container blockfull">
    <div class="block-inner-container">

        <div class="four-pics-inner">

            <h2 style='text-align:right'>כתבו עלינו

                <div class="four-pics-seperator" style='    margin: 0;'></div>
            </h2>

            <div class="row-wrap">
                <div class="four-pics-image-wrap">
                    <?php
                    while ($the_query->have_posts()) {
                        $the_query->the_post(); ?>
                        <div class="image-box">
                            <div class="image-wrap">
                                <a href="<?= get_permalink() ?>">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                            </div>

                        </div>
                    <?php
                    } ?>
                </div>
            </div>
            <?php
            if ($the_query->max_num_pages > 1) {
            ?>
                <div class="button-wrap">
                    <a href='#' data-page='2' data-max="<?php echo $the_query->max_num_pages ?>" class="load-more-about-us">
                        טען עוד
                    </a>
                </div>
            <?php
            }
            ?>
        </div>

    </div>
</div>