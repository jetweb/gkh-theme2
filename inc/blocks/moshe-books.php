<?php

$args      = ['post_type' => 'moshebooks', 'post_status' => 'publish', 'order' => 'DESC', 'posts_per_page' => 4];
$the_query = new WP_Query($args);

?>
<div class="block-container moshe-books-container four-pics-container blockfull">
    <div class="block-inner-container">

        <div class="four-pics-inner">

            <h2 style='text-align:right'>כתביו של משה פראגר

                <div class="four-pics-seperator" style='    margin: 0;'></div>
            </h2>

            <div class="row-wrap">
                <div class="four-pics-image-wrap">
                    <?php
                    while ($the_query->have_posts()) {
                        $the_query->the_post(); ?>
                        <div class="image-box">
                            <div class="image-wrap">
                                <a href="<?php the_post_thumbnail_url() ?>" data-fancybox>
                                    <?php the_post_thumbnail(); ?>
                                </a>
                            </div>
                            <div class="text-wrap">
                                <div class="image-box-title">
                                    <?php echo the_title() ?>
                                </div>
                                <div class="image-box-description">
                                    <?php the_content() ?>
                                </div>
                                <div class="image-box-date">
                                    <?php echo get_post_meta(get_the_ID())['date'][0] ?>
                                </div>
                            </div>

                        </div>
                    <?php
                    } ?>
                </div>
            </div>
            <?php
            if ($the_query->max_num_pages > 1) {
            ?>
                <div class="button-wrap">
                    <a href='#' data-page='2' data-max="<?php echo $the_query->max_num_pages ?>" class="load-more-moshe-books">
                        טען עוד
                    </a>
                </div>
            <?php
            }
            ?>
        </div>

    </div>
</div>