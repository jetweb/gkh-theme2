<?php

$image_1       = get_field('image_1');
$image_1_title = get_field('image_1_title');
$image_1_url   = get_field('image_1_url');

$image_2       = get_field('image_2');
$image_2_title = get_field('image_2_title');
$image_2_url   = get_field('image_2_url');

$image_3       = get_field('image_3');
$image_3_title = get_field('image_3_title');
$image_3_url   = get_field('image_3_url');

$image_4       = get_field('image_4');
$image_4_url   = get_field('image_4_url');
$image_4_title = get_field('image_4_title');

$title = get_field('title');

// create id attribute for specific styling
$id = 'four-pics-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>
<div class="block-container blockfull four-pics-container" id=<?=$id?>>
    <div class="block-inner-container">
        <div class="four-pics-inner">
            <h2 class="four-pics-title">
                <?php echo $title ?>
                <div class="four-pics-seperator"></div>
            </h2>
            <div class="four-pics-image-wrap">
                <div class="image-box">
                    <div class="image-wrap">
                        <?php if (!is_page_template('template-biography.php')) {?>
                        <a href="<?php echo $image_1_url; ?>">
                            <?php }?>
                            <img src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['alt']; ?>" />
                            <?php if (!is_page_template('template-biography.php')) {?>
                        </a>
                        <?php }?>
                    </div>
                    <div class="text-wrap">
                        <?php if (!is_page_template('template-biography.php')) {?>
                        <a href='<?php echo $image_1_url; ?>' class="image-box-title">
                            <?php echo $image_1_title ?>
                        </a>
                        <?php } else {?>
                        <p class="image-box-title">
                            <?php echo $image_1_title ?>
                        </p>
                        <?php }?>

                    </div>
                </div>
                <div class="image-box">
                    <div class="image-wrap">


                        <?php if (!is_page_template('template-biography.php')) {?>
                        <a href="<?php echo $image_2_url; ?>">
                            <?php }?>
                            <img src="<?php echo $image_2['url']; ?>" alt="<?php echo $image_2['alt']; ?>" />
                            <?php if (!is_page_template('template-biography.php')) {?>
                        </a>
                        <?php }?>
                    </div>
                    <div class="text-wrap">
                        <?php if (!is_page_template('template-biography.php')) {?>
                        <a href='<?php echo $image_2_url; ?>' class="image-box-title">
                            <?php echo $image_2_title ?>
                        </a>
                        <?php } else {?>
                        <p class="image-box-title">
                            <?php echo $image_2_title ?>
                        </p>
                        <?php }?>

                    </div>
                </div>
                <div class="image-box">
                    <div class="image-wrap">

                        <?php if (!is_page_template('template-biography.php')) {?>
                        <a href="<?php echo $image_3_url; ?>">
                            <?php }?>
                            <img src="<?php echo $image_3['url']; ?>" alt="<?php echo $image_3['alt']; ?>" />
                            <?php if (!is_page_template('template-biography.php')) {?>
                        </a>
                        <?php }?>
                    </div>
                    <div class="text-wrap">
                        <?php if (!is_page_template('template-biography.php')) {?>
                        <a href='<?php echo $image_3_url; ?>' class="image-box-title">
                            <?php echo $image_3_title ?>
                        </a>
                        <?php } else {?>
                        <p class="image-box-title">
                            <?php echo $image_3_title ?>
                        </p>
                        <?php }?>


                    </div>

                </div>
                <div class="image-box">
                    <div class="image-wrap">



                        <?php if (!is_page_template('template-biography.php')) {?>
                        <a href="<?php echo $image_4_url; ?>">
                            <?php }?>
                            <img src="<?php echo $image_4['url']; ?>" alt="<?php echo $image_4['alt']; ?>" />
                            <?php if (!is_page_template('template-biography.php')) {?>
                        </a>
                        <?php }?>
                    </div>
                    <div class="text-wrap">
                        <?php if (!is_page_template('template-biography.php')) {?>
                        <a href='<?php echo $image_4_url; ?>' class="image-box-title">
                            <?php echo $image_4_title ?>
                        </a>
                        <?php } else {?>
                        <p class="image-box-title">
                            <?php echo $image_4_title ?>
                        </p>
                        <?php }?>

                    </div>


                </div>
            </div>
            <!--             <?php
if ($the_query->max_num_pages > 1) {
    ?>
            <div class="button-wrap">
                <a href='#' data-page='2' data-max="<?php echo $the_query->max_num_pages ?>" class="load-more-books">
                    טען עוד
                </a>
            </div>
            <?php
}
wp_reset_postdata()?> -->

        </div>
    </div>
</div>
</div>