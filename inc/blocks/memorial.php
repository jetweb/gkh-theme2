<!-- <?php
$args      = ['post_type' => 'memorial', 'post_status' => 'publish'];
$the_query = new WP_Query($args);

?>
<div class="block-container memorial-container blockfull">
    <div class="block-inner-container">
        <div class="memorial-grid">
            <?php
while ($the_query->have_posts()) {
    $the_query->the_post();
    $memorial_meta = get_post_meta(get_the_ID());
    // print_r(get_post_meta(get_the_ID()));
    $sub_header = $memorial_meta['sub-header'][0];
    $credit     = $memorial_meta['credit'][0];
    echo '<div class="memorial-item">';
    echo '<p class="memorial-header">לזכרון עולם</p>';
    echo "<p class='memorial-sub-header'>$sub_header</p>";
    echo "<p class='memorial-main-text'>" . get_the_content() . " </p>";
    echo "<p class='memorial-credit'>$credit</p>";
    echo "<p class='memorial-footer'>ת.נ.צ.ב.ה</p>";
    echo '</div>';
    //the_content();

    the_field('date_text', get_the_ID());
}
wp_reset_postdata();

?>
        </div>
    </div>
</div> -->