<?php
// Register Custom Taxonomy
function exhibitions_cat_taxonomy()
{

    $labels = array(
        'name' => _x('Exhibitions Categories', 'Taxonomy General Name', 'gkh'),
        'singular_name' => _x('Exhibitions Category', 'Taxonomy Singular Name', 'gkh'),
        'menu_name' => __('Exhibitions Categories', 'gkh'),
        'all_items' => __('All Items', 'gkh'),
        'parent_item' => __('Parent Item', 'gkh'),
        'parent_item_colon' => __('Parent Item:', 'gkh'),
        'new_item_name' => __('New Item Name', 'gkh'),
        'add_new_item' => __('Add New Item', 'gkh'),
        'edit_item' => __('Edit Item', 'gkh'),
        'update_item' => __('Update Item', 'gkh'),
        'view_item' => __('View Item', 'gkh'),
        'separate_items_with_commas' => __('Separate items with commas', 'gkh'),
        'add_or_remove_items' => __('Add or remove items', 'gkh'),
        'choose_from_most_used' => __('Choose from the most used', 'gkh'),
        'popular_items' => __('Popular Items', 'gkh'),
        'search_items' => __('Search Items', 'gkh'),
        'not_found' => __('Not Found', 'gkh'),
        'no_terms' => __('No items', 'gkh'),
        'items_list' => __('Items list', 'gkh'),
        'items_list_navigation' => __('Items list navigation', 'gkh'),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
    );
    register_taxonomy('exhibitions_cat', array('post'), $args);

}
add_action('init', 'exhibitions_cat_taxonomy', 0);
