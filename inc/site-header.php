<?php
class GKH_header
{
    private $special_templates = ['template-future.php', 'template-contact-us-page.php'];
    public function __construct()
    {
        add_action('template_redirect', [$this, 'main']);
        remove_action('genesis_before_loop', 'genesis_do_breadcrumbs');
        add_action('genesis_entry_header', 'genesis_do_breadcrumbs', 7);
    }
    public function main()
    {
        $header_type = $this->choose_header();
    }

    private function handle_museum_header()
    {
        remove_action('genesis_entry_content', 'genesis_do_post_content');
        add_action('genesis_entry_header', 'genesis_do_post_content', 12);
        add_action('genesis_entry_header', [$this, 'gkh_museum_header_image'], 14);

        add_action('genesis_entry_header', function () {
            echo '<div class="entry-header-wrap">';
        }, 5);
        add_action('genesis_entry_header', function () {
            echo '<div class="entry-header-text-wrap">';
        }, 6);
        add_action('genesis_entry_header', function () {
            echo '</div>';
        }, 13);
        add_action('genesis_entry_header', function () {
            echo '</div>';
        }, 14);
    }

    private function handle_deafault_header()
    {
        add_action('genesis_entry_header', [$this, 'insert_header']);
    }

    private function choose_header()
    {
        if (is_front_page()) {
            return $this->handle_front_page_header();
        }
        if (is_singular('post')) {
            return $this->handle_post_page_header();
        }
        if (is_page_template('template-exhibition-categories.php')) {
            $this->handle_exhibitions_cat_header();
            return;
        }
        if (is_page_template('template-biography.php')) {
            $this->handle_biography_header();
            return;
        }
        if (is_page_template('template-tours.php')) {
            $this->handle_tours_header();
            return;
        }
        if (is_singular('display_item')) {
            $this->handle_single_display_item_header();
            return;
        }
        if (is_page_template('template-future.php')) {
            $this->handle_museum_header();
            return;
        }
        if (is_page_template('template-donations.php')) {
            $this->handle_donations_header();
            return;
        }
        if (is_page_template('template-memorial.php')) {
            $this->handle_memorial_header();
            return;
        }
        if (is_page_template('template-contact-us-page.php')) {
        remove_action('genesis_entry_header', 'insert_header');

            return;
        }
        if (is_page_template('template-no-header.php')) {
            return;
        }
        if (is_tax('videos_cat')) {
            $this->handle_videos_cat_header();
            return;
        }

        $this->handle_deafault_header();
        return;
    }

    public function insert_header()
    {
        $img_url = $this->get_header_image();
?>
        <div class="header-background" id="header-background" style="background-image: url(<?php echo $img_url ?>)"></div>
        <?php
    }

    private function get_header_image()
    {

        if (is_singular('display_item')) {

            $header_image = get_field('display_header_image', 'options');
            return $header_image['url'];
        }

        $header_image = get_the_post_thumbnail_url();
        if ($header_image) {
            return $header_image;
        }

        if (!$header_image) {
            $header_image = get_field('default_image', 'options');
            return $header_image['url'];
        }
    }
    //SINGLE DISPLAY ITEM
    public function handle_single_display_item_header()
    {

        remove_action('genesis_after_entry', 'genesis_do_author_box_single', 8);
        remove_action('genesis_after_entry', 'genesis_get_comments_template');
        remove_action('genesis_entry_header', 'genesis_do_post_title');
        add_action('genesis_before_entry_content', 'genesis_do_post_title');
        remove_action('genesis_entry_header', 'insert_header');
        add_action('genesis_after_header', [$this, 'insert_news_header'], 5);
        remove_action('genesis_entry_header', 'genesis_do_breadcrumbs', 7);
        add_action('genesis_after_header', 'genesis_do_breadcrumbs', 6);
        add_action('genesis_after_header', function () { ?>
            <div class="header-text-wrap">

                <h2>מוצגים</h2>
                <p class="display-description">
                    <?php echo get_field('display-single-header-text', 'options') ?></p>
            </div>
            </div>
        <?php
        }, 7);
    }
    //BIOGRAPHY HEADER
    public function handle_biography_header()
    {
        remove_action('genesis_entry_header', 'genesis_do_breadcrumbs', 7);
        add_action('genesis_entry_header', 'genesis_do_breadcrumbs', 8);
        //OPEN HEADER WRAP
        add_action('genesis_entry_header', function () {
        ?>
            <div class="entry-header-wrap">
                <div class="entry-header-text-wrap">

                <?php
            }, 5);
            //INSERT HEADER TEXT AND IMAGE & CLOSE WRAPPER
            add_action('genesis_entry_header', function () {
                $header_text  = get_field('header_text');
                $header_image = get_the_post_thumbnail_url();
                ?>
                    <p class="page-description">
                        <?php echo $header_text; ?>
                    </p>

                </div>
                <img src='<?php echo $header_image ?>'>
            </div>
        <?php
            }, 11);

            add_filter('body_class', function ($classes) {
                array_push($classes, 'biography');
                return $classes;
            });
        }
        //EXHIBITIONS CAT HEADER
        public function handle_exhibitions_cat_header()
        {
            add_action('genesis_entry_header', [$this, 'insert_header']);

            add_action('genesis_entry_header', 'genesis_do_post_content', 12);
            remove_action('genesis_entry_content', 'genesis_do_post_content');

            add_action('genesis_entry_header', function () {
                echo '<div class="entry-header-wrap">';
            }, 5);
            add_action('genesis_entry_header', function () {
                echo '<div class="entry-header-text-wrap">';
            }, 6);
            add_action('genesis_entry_header', function () {
                echo '</div>';
            }, 13);
            add_action('genesis_entry_header', function () {
                echo '</div>';
            }, 14);
        }
        //tours HEADER
        public function handle_tours_header()
        {
            add_action('genesis_entry_header', [$this, 'insert_header']);

            add_action('genesis_entry_header', function () {
        ?>
            <div class="header-content-wrap">
            <?php
            }, 11);

            add_action('genesis_entry_header', 'genesis_do_post_content', 12);
            remove_action('genesis_entry_content', 'genesis_do_post_content');

            add_action('genesis_entry_header', function () {
                echo '<div class="entry-header-wrap">';
            }, 5);
            add_action('genesis_entry_header', function () {
                echo '<div class="entry-header-text-wrap">';
            }, 6);
            add_action('genesis_entry_header', function () {

                $button_url = get_field('url');
                // echo '<div class="button-wrap">';
                echo "<a href='$button_url' class='header-button'>קביעת ביקור</a>";
                // echo '</div>';
                echo '</div>';
                echo '</div>';
            }, 13);
            add_action('genesis_entry_header', function () {
                echo '</div>';
            }, 14);
        }
        //MEMORIAL HEADER
        public function handle_memorial_header()
        {
            add_action('genesis_entry_header', [$this, 'insert_header']);
            add_action('genesis_entry_header', 'genesis_do_post_content', 12);
            remove_action('genesis_entry_content', 'genesis_do_post_content');

            add_action('genesis_entry_header', function () {
                echo '<div class="entry-header-wrap">';
            }, 5);
            add_action('genesis_entry_header', function () {
                echo '<div class="entry-header-text-wrap">';
            }, 6);
            add_action('genesis_entry_header', function () {
                echo '</div>';
                $button_url = get_field('donations_page', 'options');
                echo "<a href='$button_url' class='header-button'>תרומה</a>";
            }, 13);
            add_action('genesis_entry_header', function () {
                echo '</div>';
            }, 14);
        }

        //DONATIONS HEADER
        public function handle_donations_header()
        {
            add_action('genesis_entry_header', 'genesis_do_post_content', 12);
            remove_action('genesis_entry_content', 'genesis_do_post_content');
            add_action('genesis_entry_header', [$this, 'gkh_museum_header_image'], 14);
            add_action('genesis_entry_header', function () {
                echo '<div class="entry-header-wrap">';
            }, 5);
            add_action('genesis_entry_header', function () {
                echo '<div class="entry-header-text-wrap">';
            }, 6);
            add_action('genesis_entry_header', function () {
                echo '</div>';
            }, 13);
            add_action('genesis_entry_header', function () {
                echo '</div>';
            }, 14);
        }
        //VIDEOS CAT HEADER
        public function handle_videos_cat_header()
        {
            add_action('genesis_archive_title_descriptions', [$this, 'open_header_wrap'], 9);
            add_action('genesis_loop', [$this, 'close_header_wrap']);
        }
        //VIDEOS CAT HEADER
        public function open_header_wrap()
        {
            ?>
            <div class="header-wrap">
                <div class="header-wrap-inner">
                <?php
            }
            public function close_header_wrap()
            {
                echo '</div>';
                echo '</div>';
            }
            //POST HEADER
            public function handle_post_page_header()
            {
                remove_action('genesis_after_entry', 'genesis_do_author_box_single', 8);
                remove_action('genesis_after_entry', 'genesis_get_comments_template');

                add_action('genesis_after_header', [$this, 'display_news_title_header'], 7);

                remove_action('genesis_entry_header', 'insert_header');

                add_action('genesis_after_header', [$this, 'insert_news_header'], 6);

                remove_action('genesis_entry_header', 'genesis_do_breadcrumbs', 7);

                add_action('genesis_after_header', 'genesis_do_breadcrumbs', 7);
                add_action('genesis_after_header', function () {
                    echo '</div>';
                    echo '</div>';
                }, 8);
            }

            //POST
            public function insert_news_header()
            {
                if (is_singular('display_item')) {
                    $header_image = get_field('display_header_image', 'options')['url'];
                } else {
                    $header_image = get_the_post_thumbnail_url();
                }

                if (!$header_image) {

                    $header_image = get_field('default_image', 'options')['url'];
                }

                ?>
                    <div class="header-background" id="header-background">
                        <div class="header-background-inner" style="background-image: url(<?php echo $header_image ?>)">

                        </div>

                    <?php

                }
                //POST
                public function display_news_title_header()
                {
                    ?>
                        <div class="header-text-wrap">

                            <h2>חדשות</h2>



                        <?php
                    }

                    //HOME HEADER
                    public function handle_front_page_header()
                    {
                        remove_action('genesis_entry_header', 'genesis_do_post_title');
                        add_action('genesis_before_entry_content', [$this, 'bottom_new_titles_block'], 11);
                        add_action('genesis_entry_header', [$this, 'create_home_slider']);
                    }

                    //HOME SLIDER
                    public function create_home_slider()
                    {
                        $slides_amount             = get_field('slides_num');
                        $slides_amount_for_display = get_field('slides_num');
                        $slide_num                 = 1;
                        ?>
                            <div class="header-container home-carousel">
                                <?php while ($slides_amount) {
                                    $slide = get_field('slide_' . $slide_num);
                                ?>
                                    <div class="header-slide" style='background-image:url(<?php echo $slide[0]['image_' . $slide_num]['url'] ?>)'>
                                      <!--   <h4 class='slides-numbers'>
                                            <?php echo '<span class="header-first-num">' . $slide_num . '</span>/' . $slides_amount_for_display ?>

                                        </h4> -->
                                        <a href="<?php echo $slide[0]['page_link_' . $slide_num] ?>">
                                            <h2><?php echo $slide[0]['title_' . $slide_num] ?></h2>
                                        </a>
                                    </div>

                                <?php $slides_amount = $slides_amount - 1;
                                    $slide_num                                   = $slide_num + 1;
                                } ?>
                            </div>
                        <?php
                    }
                    // HOME NEWS TITLES
                    public function bottom_new_titles_block()
                    {

                        $args      = ['post_type' => 'post', 'post_status' => 'publish', 'order' => 'DESC'];
                        $the_query = new WP_Query($args);
                        //   print_r($the_query->posts);
                        ?>

                            <div class="news-titles-wrap">
                                <div class="flex-item">
                                    <p class="updates">עדכונים</p>
                                    <div class="ticker-wrap">
                                        <div class="ticker-move">
                                            <?php
                                            while ($the_query->have_posts()) {
                                                $the_query->the_post();
                                            ?>
                                                <a href='<?php echo get_post_permalink() ?>' class="single-title-wrap">
                                                    <?php
                                                    the_title();
                                                    ?>
                                                    <span>|</span>
                                                </a>
                                            <?php
                                            }
                                            wp_reset_postdata(); ?>
                                        </div>
                                    </div>
                                    <a href='<?php echo get_field('news_page', 'options')->guid ?>' class="all-updates">לכל
                                        העדכונים</a>
                                </div>
                            </div>
                        <?php
                    }

                    //MUSEUM IMAGE
                    public function gkh_museum_header_image()
                    {
                        $image = get_the_post_thumbnail_url();
                        echo '<div class="header-image-wrap">';
                        ?>
                            <img src="<?php echo $image ?>" alt="">
                    <?php
                        echo '</div>';
                    }
                }
                new GKH_header();
