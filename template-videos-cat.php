<?php
/**
 * Template Name: דף: עדויות ויזואליות-קטגוריות
 *
 */
add_action('genesis_entry_content', 'visual_categories_content');
function visual_categories_content()
{
    $terms = get_terms('videos_cat', array(
        'hide_empty' => true,
    ));
    //print_r($_SERVER);

    ?>

<div class="block-container visual-testimonies-container blockfull">

    <div class="block-inner-container">
        <div class="categories-grid">
            <?php

    foreach ($terms as $term) {
        $images = get_field('image', $term);
        $amount = get_term_meta($term->term_id)["amount"][0];
        ?><a href="<?php echo get_term_link($term) ?>">
                <div class='visual-testimonies-category'
                    style='background-image:url(<?php echo $images['sizes']['visual_categories'] ?>)'>

                    <div class="visual-text-wrap">
                        <h2><?php echo $term->name ?></h2>
                      
                    </div>

                </div>
            </a>
            <?php
}

    wp_reset_postdata();
    ?>
        </div>
    </div>
</div>
<div class="search-banner blockfull">
    <div class="block-inner-container">

        <h2>
            <?php echo get_field('visual-title', 'options') ?>
        </h2>
        <p class="search-banner-content">
            <?php echo get_field('visual-text', 'options') ?> </p>
        <div class="search-banner-buttons">
            <a href="  <?php echo get_field('visual-button-1-url', 'options') ?>  " class=''>
                <?php echo get_field('visual-button-1-text', 'options') ?> </a>
            <a href="  <?php echo get_field('visual-button-2-url', 'options') ?>  " class=''>
                <?php echo get_field('visual-button-2-text', 'options') ?> </a>
        </div>
    </div>
</div>
<?php
$image_field = get_field('image');
    $text        = get_field('text');
    ?>

<div class="block-container exhibitions-banner blockfull">
    <div class="block-inner-container flex-item">
        <div class="exhibitions-banner-text-wrap">
            <p class="pre-title">אוצרות הגנזך</p>
            <h2>תערוכות מקוונות</h2>
            <p class="exhibitions-text">
                <?php echo $text ?>
            </p>

            <a href="" class="wp-block-button__link">לכל התערוכות</a>
        </div>

        <div class="exhibitions-banner-image-wrap">
            <img src="<?php echo $image_field['sizes']['tour_thumbnail']; ?>"
                alt="<?php echo $image_field['alt']; ?>" />
        </div>
    </div>
</div>
<?php
}
genesis();