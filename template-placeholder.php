<?php

/**
 * Template Name: דף: מאמרים
 *
 */
add_action('genesis_entry_content', 'articles_page_content');
add_filter ( 'genesis_post_crumb', 'gk_breadcrumb_category_post' ); 
function gk_breadcrumb_category_post( $crumb ) {
   if(isset($_GET['kkk'])){
       return 'sdfsdf';
   }
   return $crumb;
}
function articles_page_content()
{
    $args      = ['post_type' => 'article', 'post_status' => 'publish', 'posts_per_page' => '-1'];
    $the_query = new WP_Query($args);

?>
    <div class="block-container tours-container blockfull articles">
        <div class="block-inner-container">
            <?php
            while ($the_query->have_posts()) {
                $the_query->the_post();
            ?>
            <div class="article">
                <div class="article-inner flex-item">
                    <div class="img-wrap">
                        <?php the_post_thumbnail() ?>
                    </div>
                    <div class="text-wrap">
                        <h2><?php the_title() ?></h2>
                    <?= mb_strimwidth(strip_tags(get_the_content()), 0, 600) . '... <a href="'.get_permalink().'">קרא עוד</a>'; ?>

                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <style>
        .text-wrap {
    width: 55%;
}
.img-wrap {
    width: 35%;
}
.articles .article:nth-child(even) .article-inner {
    flex-direction: row-reverse;
}
.article-inner{
    justify-content: space-between;
}
.article {
    margin: 80px 0;
}
.text-wrap a{
    color: blue;
    font-size: 20px;
    text-decoration: underline;
}
    </style>
<?php
}
genesis();
