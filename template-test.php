<?php
/**
 * Template Name: approve
 *
 */
/*  */

/* if (!isset($_GET['addData'])) {
    do_error('no_entry');
} */
//$entry = GFAPI::get_entry($_GET['addData']);
/* if (is_wp_error($entry)) {
    do_error('bad_entry');
} */

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . '/assets/css/main.css' ?>">
    <style>
    body {
        text-align: right;
        font-family: "Rubik", sans-serif;
        font-size: 18px;
        font-weight: 500;
        line-height: 1.39;
    }

    img {
        margin: 0 auto;
        width: 115px;
    }

    iframe {
        border: none;
    }

    .donor-name {
        font-size: 24px;
        font-weight: 500;
        line-height: 1.13;
    }
    </style>
</head>

<body>
    <div id='success-message' class="success-message">

        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/donation-v.png' ?>" />
        <?php
        /* 
        $amount      = rgar($entry, '1');
        $real_amount = floor($_REQUEST['total'] / 100);

        $entry[16] = $_GET['tranId'];
        $entry[17] = 'שולם';
        $entry[1]  = $_REQUEST['total'];
        $update = GFAPI::update_entry($entry); */
        echo 'תרומתך התקבלה בהצלחה!<br>';
        echo get_field('donation_success', 'options');

        ?>
    </div>
</body>


</html>