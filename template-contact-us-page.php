<?php
/**
 * Template Name: דף- צור קשר
 *
 */
/* apply_filters( 'genesis_post_title_text', get_the_title() );*/
add_action('genesis_entry_content', 'contact_us_box');
add_action('genesis_after_entry', 'create_bottom_banner');
function contact_us_box()
{
    ?>
<div class="contact-us-box ">
    <div class="contact-us-header">
        צור קשר
    </div>
    <div class="form-wrap">
        <?php $form = get_field('gravity_form');
    gravity_form($form, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex = 20, $echo = true);?>
    </div>
</div>


<?php
}
function create_bottom_banner()
{
    ?>

<div class="bottom-banner ">
    <div class="bottom-text">
        אנו מזמינים אתכם לקבוע ביקור בגנזך קידוש השם
    </div>
    <a href="<?php echo get_field('visitor_info_page', 'options') ?>">תרומה</a>
</div> <?php
}
genesis();