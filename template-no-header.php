<?php

/**
 * Template Name: ללא הדר
 *
 */
add_action('wp_head', function () {
?>
    <style>
        .entry-header {
            display: none;
        }

        #main-content {
            background-image: url(<?= get_field('image')['url'] ?>);
            background-position: center;
        }

        .entry-content>*:first-child,
        .block-area>*:first-child {

            margin-bottom: 0 !important;
        }

        .entry-content {
            background: #ffffffba;
            padding: 50px 16px;
        }
    </style>
<?php
});
genesis();
