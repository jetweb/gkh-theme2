<?php
/**
 * Template Name: deny
 *
 */
/* wp_enqueue_style('ea-fonts', ea_theme_fonts_url());
function ea_theme_fonts_url()
{
    return 'https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet';
} */
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . '/assets/css/main.css' ?>">
    <style>
    body {
        color: #fff;
        text-align: right;
        font-family: "Rubik", sans-serif;
        font-size: 18px;
        font-weight: 500;
        line-height: 1.39;
    }

    img {
        margin: 0 auto;
        width: 115px;
    }

    iframe {
        border: none;
    }

    .donor-name {
        font-size: 24px;
        font-weight: 500;
        line-height: 1.13;
    }
    </style>
</head>

<body>

    <?php echo get_field('donation_failure', 'options');
    ?>
</body>


</html>