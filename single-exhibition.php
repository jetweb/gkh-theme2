<?php
add_action('genesis_entry_content', 'display_exhibition_items');
remove_action('genesis_entry_content', 'genesis_do_post_content');


function clear_br($content){
		return str_replace("&nbsp;","<br />", $content);
	}
	add_filter('the_content', 'clear_br');
function display_exhibition_items()
{
   $content=mb_strimwidth(get_the_content(), 0, 500);
    echo '<div class="partial-content">'. $content.'...<span class="exhibition-read-more" style="color:#ffd65d;cursor:pointer">[קרא עוד]</span></div>';
    ?>
        <div class="full-content">
            <?= str_replace("&nbsp;","<br />", get_the_content()) ?>
        </div>
    <?php
    if (have_rows('items')) :
?>
        <div class="items">

            <?php
            $num = 1;
            while (have_rows('items')) : the_row();
                $type   = get_sub_field('type');
                $width  = get_sub_field('width');
                $height = get_sub_field('height');
                //BACKGROUND IMAGE

                if ($type != 'document') {
                    $image_url  = get_item_backgroud($type);
                    $background = "background-image:url('$image_url')";
                } else {
                    $background = '';
                }
                //CONTENT
                if ($type == 'document') {
                    $content = '<p class="item-content">' . get_sub_field('text') . '</p>';
                    //  $file    = get_sub_field('document');
                    $pdf_link    = get_sub_field('pdf_link');
                } else if ($type == 'story') {
                    $content      = '<p class="item-content">' . get_sub_field('text') . '</p>';
                    $full_content = '<p class="item-content">' . get_sub_field('story')->post_content . '</p>';
                } else if ($type == 'testimony' || $type == 'video') {
                    $content = "<img class='play-logo' src='" . get_stylesheet_directory_uri() . '/assets/images/play.png' . "'>";
                } else if ($type == 'image') {
                    $content = '<p class="item-content">' . get_sub_field('text') . '</p>';
                }else {
                    $content = '';
                }
                //TITLE
                if ($type == 'video' || $type == 'document' || $type == 'image' || $type == 'book' || $type == 'link') {
                    $title = get_sub_field('title');
                } else if ($type == 'testimony' || $type == 'story') {
                    $title = get_sub_field($type)->post_title;
                } else {
                    $title = '';
                }
                //VIDEO
                if ($type == 'video') {
                    $video_url = get_sub_field('video');
                } else if ($type == 'testimony') {
                    $video_url = get_post_meta(get_sub_field('testimony')->ID)['video'][0];
                }
                //FANCY BOX PARAMETERS
                //if ($type != 'document') {
                if ($type == 'image') {
                    // $fancybox = "data-fancybox='gallery'  href='$image_url'";
                    $fancybox = 'data-fancybox data-src="#hidden-content-' . $num . '" href="javascript:;"';
                } else if ($type == 'video' || $type == 'testimony') {
                    $fancybox = "data-fancybox  href='$video_url'";
                } else if ($type == 'document') {

                    $fancybox = 'data-fancybox data-type="iframe" data-src="' . $pdf_link . '" href="javascript:;"';
                
                } else if ($type == 'link') {
                    $link=get_sub_field('link');                    
                    $fancybox = "href='$link' target='blank'";
                } else if ($type == 'book') {
                    $fancybox = "data-book=df_".get_sub_field('book_id');
                } else {
                    $fancybox = 'data-fancybox data-src="#hidden-content-' . $num . '" href="javascript:;"';
                }
             
                if ($type == 'book') {
            ?>
                    <div class="book-wrap" style='display:none'>
                        <?php
                        echo do_shortcode('[dflip  type="thumb" id="' . get_sub_field('book_id') . '" ][/dflip]');
                        ?>

                    </div>
                    <?php
                }
                ?>

                <a <?php echo $fancybox ?> class="item <?php echo $type ?> span-row-<?php echo $height ?> span-col-<?php echo $width ?>" style=<?php echo $background ?>>

                    <h5> <?php echo $title; ?></h5>
                    <?php
                    if ($type != 'image') {
                        echo $content;
                    }

                    if ($type == 'document') { ?>
                        <span class="download">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/download-yellow.png' ?>" alt="">
                        </span>
                    <?php
                    }
                    ?>
                </a>
                <?php
                if ($type != 'document' && $type != 'book') {
                ?>
                    <div style="display: none;" class='exhibitions-modal' id="hidden-content-<?php echo $num ?>">
                        <?php
                        if ($type == 'story') {
                        ?>
                            <h5> <?php echo $title; ?></h5>
                        <?php echo $full_content;
                        } else if ($type == 'image') {
                        ?>
                            <img src="<?php echo $image_url ?>" alt="">
                            <div class='image-modal-title'><?php echo $title ?></div>
                        <?php
                            echo $content;
                        } else if ($type == 'document') {
                        }
                        ?>

                    </div>
            <?php
                }
                $num = $num + 1;
            endwhile; ?>
        </div>
<?php
    endif;
}
function get_item_backgroud($type)
{
    if ($type == "story" || $type == "testimony" ) {
        return get_the_post_thumbnail_url(get_sub_field($type));
    } else {
        return get_sub_field('image')['url'];
    }
}

genesis();
