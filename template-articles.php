<?php

/**
 * Template Name: articles
 */
add_action('genesis_entry_content', 'articles_page_content');
function articles_page_content()
{
    $args      = ['post_type' => 'article', 'post_status' => 'publish', 'posts_per_page' => '-1'];
    $the_query = new WP_Query($args);

?>
    <div class="block-container tours-container blockfull articles">
        <div class="block-inner-container">
            <?php
            while ($the_query->have_posts()) {
                $the_query->the_post();
            ?>
            <div class="article-gkh">
                <div class="article-inner flex-item">
                    <div class="img-wrap">
                        <?php the_post_thumbnail() ?>
                    </div>
                    <div class="text-wrap">
                        <h2><?php the_title() ?></h2>
                    <?= mb_strimwidth(strip_tags(get_the_content()), 0, 600) . '... <a href="'.get_permalink().'">קרא עוד</a>'; ?>

                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <style>
        .text-wrap {
    width: 55%;
}
.img-wrap {
    width: 35%;
}
.articles .article-gkh:nth-child(even) .article-inner {
    flex-direction: row-reverse;
}
.article-inner{
    justify-content: space-between;
}
.article-gkh {
    margin: 80px 0;
}
.text-wrap a{
    color: blue;
    font-size: 20px;
    text-decoration: underline;
}
    </style>
<?php
}
genesis();
