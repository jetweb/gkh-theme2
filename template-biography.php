<?php

/**
 * Template Name: biography
 *
 */

add_action('genesis_entry_content', 'print_books', 20);

function print_books()
{
    $args      = ['post_type' => 'moshebooks', 'post_status' => 'publish', 'posts_per_page' => 4];
    $the_query = new WP_Query($args);
?>
    <h2 class="four-pics-title">
        כתביו של משה פראגר
    </h2>
    <div class="block-container productions-container" data-micromodal-trigger="modal">
        <div class="block-inner-container">
            <div class="productions-grid">
                <?php
                $num = 1;
                while ($the_query->have_posts()) {
                    $the_query->the_post();

                ?>
                    <div class="production-item">
                        <a data-fancybox='gallery' data-src="#hidden-content-<?php echo $num ?>" href="javascript:;">
                            <div class="production-item-inner" data-src="#hidden-content">
                                <div class="production-image-wrap">
                                    <?php the_post_thumbnail('productions'); ?>
                                </div>
                            </div>
                        </a>
                        <div class="production-title"><?php the_title(); ?>
                        </div>
                        <p class="image-box-date">
                            <?php the_field('date') ?>
                        </p>

                    </div>

                    <div style="display: none;" class='modal' id="hidden-content-<?php echo $num ?>">
                        <div class="flex-item">
                            <div class="modal-image-wrap">
                                <?php the_post_thumbnail('productions'); ?>
                            </div>
                            <div class="modal-text-wrap">
                                <h2><?php the_title(); ?></h2>
                                <h3><?php the_field('sub_title'); ?></h3>
                                <div class="modal-content">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                    $num = $num + 1;
                }


                wp_reset_postdata();

                ?>

            </div>
            <?php
            if ($the_query->max_num_pages > 1) {
            ?>
                <div class="load-more-button-wrap">
                    <a href='#' data-page='2' data-max="<?php echo $the_query->max_num_pages ?>" class="load-more-moshe-books">
                        טען עוד
                    </a>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
<?php
}

genesis();
