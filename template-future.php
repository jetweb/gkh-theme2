<?php
/**
 * Template Name: Future Museum
 *
 */

/* remove_action('genesis_entry_header', 'insert_header');
add_action('genesis_entry_header', 'genesis_do_post_content', 12);
remove_action('genesis_entry_content', 'genesis_do_post_content');
add_action('genesis_entry_header', 'gkh_museum_header_image', 14);
add_action('genesis_entry_header', function () {
echo '<div class="entry-header-wrap">';
}, 5);
add_action('genesis_entry_header', function () {
echo '<div class="entry-header-text-wrap">';
}, 6);
add_action('genesis_entry_header', function () {
echo '</div>';
}, 13);
add_action('genesis_entry_header', function () {
echo '</div>';
}, 14);

function gkh_museum_header_image()
{
$image = get_field('header_image');
echo '<div class="header-image-wrap">';
?>
<img src="<?php echo $image['url'] ?>" alt="">
<?php
echo '</div>';
} */
function do_museum_content()
{
    $num = 1;
    if (have_rows('agafs')):
    ?>
<div class="block-area">


    <div class="agafs-header-wrap">

        <h2>אגפי המוזיאון</h2><br>
        <span class="sub-title">המוזיאון העתידי יכלול את האגפים הבאים:</span>
    </div>
    <?php
while (have_rows('agafs')): the_row();
        ?>
    <div class="agafs blockfull">
        <div class="block-inner-container">
            <section class="agaf flex-item">
                <div class="agaf-text-wrap">
                    <?php
    echo '<span class="agaf-number agaf-number-' . $num . '">' . get_sub_field('number') . '</span>';
        echo '<h2>' . get_sub_field('title') . '</h2>';
        echo '<p class="agaf-content">' . get_sub_field('content') . '</p>';

        ?>
                </div>
                <div class="agaf-image-wrap">
                    <?php echo wp_get_attachment_image(get_sub_field('image')['ID'], 'large'); ?>

                </div>

            </section>
        </div>
    </div>
    <?php
    $num = $num + 1;
    endwhile;
    ?>
</div>

<?php
else:

        // no rows found

    endif;
}
add_action('genesis_after_loop', 'do_museum_content');
genesis();