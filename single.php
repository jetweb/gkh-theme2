<?php
/**
 * Single Post
 *
 * @package      EAGenesisChild
 * @author       Bill Erickson
 * @since        1.0.0
 * @license      GPL-2.0+
**/

// Entry category in header
add_action( 'genesis_entry_header', 'ea_entry_category', 8 );
add_action( 'genesis_entry_header', 'ea_entry_author', 12 );
add_action( 'genesis_entry_header', 'ea_entry_header_share', 13 );



function ea_entry_header_share() {
	do_action( 'ea_entry_header_share' );
}


remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );
remove_action( 'genesis_after_entry', 'genesis_do_author_box_single', 8 );

genesis();
