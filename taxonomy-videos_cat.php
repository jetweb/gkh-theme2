<?php

add_action('genesis_before_while', function () {
?>
    <div class="content-wrap">
    <?php
}, 6);
//add_action('genesis_before_while', 'add_video_tag', 8);
add_action('genesis_before_while', 'open_articles_wrap');
add_action('genesis_after_while', 'close_articles_wrap');
add_action('genesis_before_footer', 'search_banner', 7);
add_action('genesis_after_content_sidebar_wrap', function () {
    ?>
    </div>
<?php
});

function add_video_tag()
{
    $first_videos = get_posts(array(
        'post_type'   => 'visual_testimony',
        'numberposts' => 1,
        'category'    => get_queried_object()->term_id,

    ));

    $first_video = $first_videos;
    $video_url   = get_field('video', $first_video);

?>

    <div class="video-wrap">

        <h3><?php the_title() ?></h3>
        <video controls controlsList="nodownload" autoplay>

            <source class='video-source' src='<?php echo $video_url ?>' type='video/mp4'>
        </video>
        <p class="pre-title">עדויות נוספות</p>
    </div>
<?php
}
function open_articles_wrap()
{
    echo '<div class="videos-slider-wrap">';

    // echo '<div class="variable-width flex-item">';
}
function close_articles_wrap()
{
    // echo '</div>';
    echo '</div>';
}
function search_banner()
{ ?>

    <div class="search-banner blockfull">
        <div class="block-inner-container">

            <h2>
                <?php echo get_field('visual-title', 'options') ?>
            </h2>
            <p class="search-banner-content">
                <?php echo get_field('visual-text', 'options') ?> </p>
            <div class="search-banner-buttons">
                <a href="  <?php echo get_field('visual-button-1-url', 'options') ?>  " class=''>
                    <?php echo get_field('visual-button-1-text', 'options') ?> </a>
                <a href="  <?php echo get_field('visual-button-2-url', 'options') ?>  " class=''>
                    <?php echo get_field('visual-button-2-text', 'options') ?> </a>
            </div>
        </div>
    </div>

<?php
}
genesis();
