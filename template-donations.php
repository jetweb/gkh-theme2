<?php
/**
 * Template Name: תרומות
 *
 */
/* remove_action('genesis_entry_header', 'insert_header');
add_action('genesis_entry_header', 'genesis_do_post_content', 12);
remove_action('genesis_entry_content', 'genesis_do_post_content');
add_action('genesis_entry_header', 'gkh_museum_header_image', 14);
add_action('genesis_entry_header', function () {
echo '<div class="entry-header-wrap">';
}, 5);
add_action('genesis_entry_header', function () {
echo '<div class="entry-header-text-wrap">';
}, 6);
add_action('genesis_entry_header', function () {
echo '</div>';
}, 13);
add_action('genesis_entry_header', function () {
echo '</div>';
}, 14); */
add_action('genesis_after_loop', 'display_donations_content');

function gkh_museum_header_image()
{
    $image = get_field('header_image');
    echo '<div class="header-image-wrap">';
    ?>
<img src="<?php echo $image['url'] ?>" alt="">
<?php
echo '</div>';
}

function display_donations_content()
{
    ?>
<div class="donations-box">
    <div class="donations-first-screen">
        <div class="donations-box-title">
            תרומה לגנזך קידוש השם
        </div>
        <div class="dontaions-box-amount">
            <p>כמה תרצו לתרום?</p>
            <div class="amount-circle-wrap">
                <div class="amount-circle">₪20</div>
                <div class="amount-circle">₪50</div>
                <div class="amount-circle ">₪100</div>
                <div class="input-wrap">
                    <input type=number class="amount-circle custom-amount" placeholder='₪?'>

                    </input>
                    <span>סכום אחר
                    </span>
                </div>
            </div>
            <span class="error-message">יש לבחור סכום</span>
        </div>
        <div class="donations-method">

            <div class="method-buttons">
                <div class='method-bank-transfer'>המשך</div>

            </div>
        </div>
        <div class="donations-bottom-info">
            <div class="donations-bottom-info-inner">
                <span>ניתן לתרום גם באמצעות:</span>

                <div class="info-wrap-small">
                    <div>
                        הדואר
                        <p>ת"ד 242, בני ברק</p>
                    </div>
                    <div>
                        חשבון הבנק
                        <p>בנק פאג"י, סניף הרב לנדא בני ברק, מס' ח-ן 52-188-409000833</p>
                    </div>
                    <div>הנצחה
                        <p>הנציחו את שמות יקיריכם שניספו בשואה, את זכר קהילתכם שחרבה, את זכר מרכזי התורה
                            והחסידות שנכחדו בשואה במרכז היהודי החדש</p>
                    </div>
                    <div>טלפונית
                        <p>03-5795589; 03-5703018</p>
                    </div>
                </div>

                <div class="info-wrap-big">
                    <div class="right-box">
                        <p>הדואר</p>
                        <p>חשבון הבנק</p>
                        <p>הנצחה</p>
                        <p>טלפונית</p>
                    </div>
                    <div class="left-box">
                        <p>ת"ד 242, בני ברק</p>
                        <p>בנק פאג"י, סניף הרב לנדא בני ברק, מס' ח-ן 52-188-409000833 </p>
                        <p>הנציחו את שמות יקיריכם שניספו בשואה, את זכר קהילתכם שחרבה, את זכר מרכזי התורה והחסידות שנכחדו
                            בשואה במרכז היהודי החדש
                        </p>
                        <p>03-5795589; 03-5703018</p>

                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="donations-second-screen">
        <div class="donations-box-title">
            תרומה לגנזך קידוש השם
        </div>
        <?php $form = get_field('gravity_form');
    gravity_form($form, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex = 20, $echo = true);?>
    </div>
</div>
<?php
}
genesis();