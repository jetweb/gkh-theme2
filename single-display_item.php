<?php

/*  add_action('genesis_entry_header', function () {?>
<div class="header-text-wrap">

    <h2>מוצגים</h2>
    <p class="display-description">גנזך קידוש השם אוצר מוצגים נדירים מתקופת השואה. <br>
        ניתן להגיע אל מוזאון הגנזך ולהתרשם מהמוצגים במציאות</p>
</div>
</div>
<?php
}); */
/* remove_action('genesis_entry_header', 'genesis_do_post_title');
add_action('genesis_before_entry_content', 'genesis_do_post_title');  */
/* add_action('genesis_entry_header', 'insert_header');

function insert_header()
{

$header_image = get_field('display_header_image', 'options');

?>
<div class="header-background" id="header-background" style="background-image: url(<?php echo $header_image['url'] ?>)">

    <?php

} */

add_action('genesis_before_footer', function () {
    ?>
    <div class="display-items-banner blockfull"
        style="background-image:url(' <?php echo get_field('items-giveaway-bg-image', 'options')['url'] ?>')">
        <div class="block-inner-container">

            <h2>
                <?php echo get_field('items-giveaway-title', 'options') ?></h2>
            <p class="display-items-content">
                <?php echo get_field('items-giveaway-text', 'options') ?>
            </p>
            <div class="display-items-buttons">
                <a href="<?php echo get_field('items-giveaway-button-url', 'options') ?>">
                    <?php echo get_field('items-giveaway-button-text', 'options') ?></a>
            </div>
        </div>
    </div>
    <?php
}, 5);
genesis();