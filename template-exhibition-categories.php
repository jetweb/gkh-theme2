<?php
/**
 * Template Name: Exhibitions
 *
 */

add_action('genesis_loop', 'exhibitions_loop');
function exhibitions_loop()
{
    $args      = ['post_type' => 'exhibition', 'post_status' => 'publish','posts_per_page'=>-1];
    $the_query = new WP_Query($args);
    ?>
<div class="block-container exhibitions-container blockfull">
    <div class="block-inner-container">
        <div class="exhibitions-grid">
            <?php

    while ($the_query->have_posts()) {
        $the_query->the_post();

        ?>
            <a href="<?php echo get_post_permalink() ?>" class="exhibition-item"
                style='background-image:url(<?php echo the_post_thumbnail_url() ?> )'>

                <div class="exhibition-title"><?php the_title();?>
                </div>
            </a>



            <?php

    }
    wp_reset_postdata();

    ?>

        </div>
    </div>
</div>

<?php

}
genesis();