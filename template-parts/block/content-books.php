<?php
global $post;
$args = ['post_type' => 'book', 'post_status' => 'publish'];
$the_query = new WP_Query($args);
?>
<div class="block-container books-container">
<!--<p class="books-block-title">הוצאה לאור ״גנזך קידוש השם״</p>
<p class="books-block-description">גנזך קידוש השם מוציא לאור ספרים בנושאים שונים עבור מוסדות חינוך ועבור כלל הציבור כדי להנגיש באמצעותם לציבור את העדויות הרבות והארוכות השמורות בו. הספרים שבהפקת "גנזך קידוש השם" זוכים לביקורות אוהדות ביותר מצופים וממחנכים.</p>-->
<div class="books-grid">
<?php
while ($the_query->have_posts()) {
    $the_query->the_post();
    echo '<div class="book-item">';
    the_post_thumbnail('tour_thumbnail');
    the_title();
    echo '</div>';
    // the_content();

    // the_field('date_text', get_the_ID());
}
wp_reset_postdata();

?>
</div>
</div>

