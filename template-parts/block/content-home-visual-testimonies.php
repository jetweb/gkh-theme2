<?php
global $post;
//echo '<prE>';
$args = ['post_type' => 'visual_testimony', 'post_status' => 'publish', 'order' => 'ASC', 'posts_per_page' => 4];
$the_query = new WP_Query($args);
?>
<div class="block-container blockfull home-testimonies-container ">
<div class="home-testimonies-inner">
<h2 class="home-testimonies-title">
עדויות ויזואליות
</h2>

<div class="home-testimonies-text">

<p class="home-testimonies-inner-text">גנזך קידוש השם בתמיכת מינהל התרבות, ועידת התביעות, קרן העזבונות והחברה להשבת רכוש עוסק באופן אינטנסיבי בגביית עדויות מניצולי שואה על ידי צוות תחקירנים שהוכשר לכך</p>
<div class="home-testimonies-buttons">
<a href="">למסירת עדות</a>
<a href="">לכל העדויות</a>
</div>
</div>
<div class="home-testimonies-images" id='home-testimonies-images'>
<?php
while ($the_query->have_posts()) {
    $the_query->the_post();
    echo '<div class="home-testimony-item">';
    the_post_thumbnail('tour_thumbnail');
    echo '<div class="testimonies-description">';
    the_title();
    echo '</div>';
    echo '</div>';

}
wp_reset_postdata();

?>
</div>
</div></div>


