<?php
/**
 * Block Name: Testimonial
 *
 * This is the template that displays the testimonial block.
 */

// get image field (array)
$image_1 = get_field('image_1');
$image_1_title = get_field('image_1_title');
$image_1_description = get_field('image_1_description');
$image_1_date = get_field('image_1_date');
$image_2 = get_field('image_2');
$image_2_title = get_field('image_2_title');
$image_2_description = get_field('image_2_description');
$image_2_date = get_field('image_2_date');
$image_3 = get_field('image_3');
$image_3_title = get_field('image_3_title');
$image_3_description = get_field('image_3_description');
$image_3_date = get_field('image_3_date');
$image_4 = get_field('image_4');
$image_4_title = get_field('image_4_title');
$image_4_description = get_field('image_4_description');
$image_4_date = get_field('image_4_date');
$title = get_field('title');

// create id attribute for specific styling
$id = 'four-pics-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>
<div class="block-container blockfull four-pics-container" id=<?=$id?>>
<div class="four-pics-inner">
<h2 class="four-pics-title">
<?php echo $title ?>
</h2>
<div class="four-pics-seperator"></div>
<div class="four-pics-image-wrap">
<div class="image-box">
<img src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['alt']; ?>" />
<p class="image-box-title">
<?php echo $image_1_title ?>
</p>
<p class="image-box-description">
<?php echo $image_1_description ?>
</p>
<p class="image-box-date">
<?php echo $image_1_date ?>
</p>
</div>
<div class="image-box">
<img src="<?php echo $image_2['url']; ?>" alt="<?php echo $image_2['alt']; ?>" />
<p class="image-box-title">
<?php echo $image_2_title ?>
</p>
<p class="image-box-description">
<?php echo $image_2_description ?>
</p>
<p class="image-box-date">
<?php echo $image_2_date ?>
</p>
</div>
<div class="image-box">
<img src="<?php echo $image_3['url']; ?>" alt="<?php echo $image_3['alt']; ?>" />
<p class="image-box-title">
<?php echo $image_3_title ?>
</p>
<p class="image-box-description">
<?php echo $image_3_description ?>
</p>
<p class="image-box-date">
<?php echo $image_3_date ?>
</p>
</div>
<div class="image-box">
<img src="<?php echo $image_4['url']; ?>" alt="<?php echo $image_4['alt']; ?>" />
<p class="image-box-title">
<?php echo $image_4_title ?>
</p>
<p class="image-box-description">
<?php echo $image_4_description ?>
</p>
<p class="image-box-date">
<?php echo $image_4_date ?>
</p>
</div>
</div>
</div>
</div>