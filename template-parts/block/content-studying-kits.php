<?php
global $post;

$args = ['post_type' => 'studying_kit', 'post_status' => 'publish'];
$the_query = new WP_Query($args);
?>
<div class="block-container studying-kits-container">
<!-- <p class="studying-kits-block-title">ערכות לימוד</p>
<p class="studying-kits-block-description">גנזך קידוש השם הפיק סדרה של חוברות לימוד, המבוססים על התחקירים והאוצרות שברשותו. חוברות הלימוד מאפשרות לציבור המבקרים, כמו-גם לכל הגולשים ברשת, גישה למקורות הארכיונים ,למטרות של הוראה, ולפעילות אינטראקטיבית.

</p> -->
<div class="studying-kits-grid">
<?php
while ($the_query->have_posts()) {
    $the_query->the_post();
    echo '<div class="studying-kits-item">';
    echo '<div class="studying-kits-item-text">';
    the_title();
    the_content();
    echo '<p class="studying-kits-includes">הערכה כוללת</p>';
    echo '<ul class="studying-kits-features">';
    echo '<li class="studying-kits-features-item">';
    the_field('feature_1', get_the_ID());
    echo '</li>';
    echo '<li class="studying-kits-features-item">';
    the_field('feature_2', get_the_ID());
    echo '</li>';
    echo '<li class="studying-kits-features-item">';
    the_field('feature_3', get_the_ID());
    echo '</li>';
    echo '</ul>';
    echo '<div class="studying-kit-download">הורדת פרק לדוגמא';
    echo '<span class="download"></span>';
    echo '</div>';
    echo '</div>';
    echo '<div class="studying-kits-item-image">';
    the_post_thumbnail('tour_thumbnail');

    echo '<div>';
    echo '<div>';
    // the_content();

    // the_field('date_text', get_the_ID());
}
wp_reset_postdata();

?>
</div>
</div>

