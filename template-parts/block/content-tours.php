<?php
global $post;

$args = ['post_type' => 'tour', 'post_status' => 'publish'];
$the_query = new WP_Query($args);

?>
<div class="block-container tours-container">
<div class="tours-top-box">
<div class="tours-image-box">



</div>
<div class="tours-text-box">
<p class="tours-pre-title">סיורים לימודיים</p>
<p class="tours-title"> </p> <!--PUT TITLE-->
<p class="tours-date"></p>
<p class="tours-content"></p>
</div>
</div>
<div class="tours-bottom-box">
<?php
while ($the_query->have_posts()) {
    $the_query->the_post();
    the_post_thumbnail('tour_thumbnail');
    the_title();

    // the_content();

    // the_field('date_text', get_the_ID());
}
wp_reset_postdata();

?>
</div>
</div>

