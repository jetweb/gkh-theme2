<?php
global $post;
$args = ['post_type' => 'production', 'post_status' => 'publish'];
$the_query = new WP_Query($args);
?>
<div class="block-container productions-container">
<!-- <p class="productions-block-title">הוצאה לאור ״גנזך קידוש השם״</p>
<p class="productions-block-description">"גנזך קידוש השם" מפיק סרטים בנושאים שונים עבור מוסדות חינוך ועבור כלל הציבור כדי להנגיש באמצעותם לציבור את העדויות הרבות והארוכות השמורות בו. עד כה הופקו סרטים בנושאי העבר היהודי, קידוש השם בשואה ושיקומה הרוחני של שארית הפליטה, בניסיון להעביר את המסר היהודי-ייחודי של "גנזך קידוש השם" בצורה אמינה, מרתקת וחווייתית. הסרטים שבהפקת "גנזך קידוש השם" זוכים לביקורות אוהדות ביותר מצופים וממחנכים, ומהווים את אחד הפרויקטים החינוכיים-תיעודיים המצליחים ביותר
</p> -->
<div class="productions-grid">
<?php
while ($the_query->have_posts()) {
    $the_query->the_post();
    echo '<div class="production-item">';
    the_post_thumbnail('tour_thumbnail');
    the_title();
    echo '<div>';
    // the_content();

    // the_field('date_text', get_the_ID());
}
wp_reset_postdata();

?>
</div>
</div>

