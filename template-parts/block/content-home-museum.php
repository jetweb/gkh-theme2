<div class="block-container home-future-museum blockfull">
<div class="block-inner-container flex-item">
<div class="top-div-image">
<img src="../wp-content/themes/gkh/assets/images/home-museum.png" alt=""> 
</div>
<div class="top-div-text text-wrap">
<h2 class="top-div-title">מוזיאון עתידי</h2>
<p class="top-div-description">גנזך קידוש השם רואה את המוזיאון העתידי ככלי להרחיב ולהעמיק את המידע ואת המודעות לנושא השואה, תוך הדגשתה של התגובה היהודית וגילויי העמידה הרוחנית תחת רדיפות הנאצים</p>
<a href="" class="read-more">המשך קריאה</a>
</div>
</div>
</div>



