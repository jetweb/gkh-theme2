<?php
echo '<pre>';
global $post;
/* $args = ['post_type' => 'visual_testimony', 'post_status' => 'publish'];
$the_query = new WP_Query($args);
 */

$terms = get_terms('videos_cat', array(
    'hide_empty' => false,
));

?>
<div class="block-container visual-testimonies-container">
<!--<p class="visual-testimonies-title">אוסף עדויות</p>
<p class="visual-testimonies-description">"גנזך קידוש השם" בתמיכת מינהל התרבות, ועידת התביעות, קרן העזבונות והחברה להשבת רכוש עוסק באופן אינטנסיבי בגביית עדויות מניצולי שואה על ידי צוות תחקירנים שהוכשר לכך. עד היום נגבו מאות רבות של עדויות המספרות את סיפור האמונה וניצחונה של הרוח. פעילות זו היא פעילות הצלה של ממש, כיוון שככל שחולפות השנים מתמעטים הניצולים ומתקשים יותר לתת את עדותם.
</p>-->
<div class="categories-grid">
<?php

foreach ($terms as $term) {
    echo '<div class="visual-testimonies-category">';
    $images = get_field('image', $term);
    echo "<img src='$images[url];' class='$term->slug-image' alt=' $images[alt];' />";
    echo $term->name;
    echo '</div>';

}

wp_reset_postdata();
?>
</div>
</div>
