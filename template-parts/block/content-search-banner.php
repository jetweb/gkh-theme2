<div class="search-banner">
<p class="search-banner-title">
חיפוש עדות
</p>
<p class="search-banner-content">
"גנזך קידוש השם" עוסק באופן אינטנסיבי בגביית עדויות מניצולי שואה על ידי צוות תחקירנים שהוכשר לכך. עד היום נגבו מאות רבות של עדויות המספרות את סיפור האמונה וניצחונה של הרוח.
</p>
<div class="search-banner-buttons">
<a href="">לחיפוש במאגר מורחב</a>
<a href="">תיאום ראיון עדות</a>
</div>
</div>
