<?php
echo '<pre>';
$images = get_field('gallery');
//var_dump(get_field('gallery'));

?>

<div class="block-container gallery-container">
<div class="top-container">
<div class="gallery-big-image">
<img src="<?php echo $images[0]['url']; ?>" alt="<?php echo $images[0]['alt']; ?>" />

</div>
<div class="gallery-image-info">
<p class="gallery-pre-title">ר' משה פראגר</p>
<p class="gallery-title">אלבום תמונות</p>
<p class="gallery-image-year"><?php echo $images[0]['caption'] ?></p>
<p class="gallery-image-description"><?php echo $images[0]['description'] ?></p>
</div>
</div>
<div class="bottom-container"></div>
<?php foreach ($images as $img) {
    echo "<img src='$img[url]' alt='$img[alt]'/>";

}

?>
</div>
