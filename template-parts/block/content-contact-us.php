<div class="block-container contact-us-container" style='background-image:url("contact-us-background.png")'>
<div class="fields-box">
<form action="">
<label class="contact-us-title">צרו קשר</label>
<label class="contact-us-subtitle">עזרו למוסד להמשיך במפעליו החשובים להנצחת השואה</label>
<input type="text" name='name' class='name-input' placeholder='שם מלא'>
<input type="text" name='email' class='email-input' placeholder='דוא"ל'>
<textarea name="content" class='content-input' id="" cols="30" rows="10"></textarea>
<input type="submit" value="שליחת פניה" class='submit-button'>
</form>

</div>
<div class="contact-us-right-box">
<p class="right-box-title">מוזיאון עתידי</p>
<p class="right-box-description">גנזך קידוש השם רואה את המוזיאון העתידי ככלי להרחיב ולהעמיק את המידע ואת המודעות לנושא השואה, תוך הדגשתה של התגובה היהודית וגילויי העמידה הרוחנית תחת רדיפות הנאצים</p>
<a href="" class="read-more-button">המשך קריאה</a>
</div>
</div>
