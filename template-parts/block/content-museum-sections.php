<?php
/**
 * Block Name: Testimonial
 *
 * This is the template that displays the testimonial block.
 */

// get image field (array)
$image_1 = get_field('image_1');
$image_1_title = get_field('image_1_title');
$image_1_description = get_field('image_1_description');
$image_2 = get_field('image_2');
$image_2_title = get_field('image_2_title');
$image_2_description = get_field('image_2_description');
$image_3 = get_field('image_3');
$image_3_title = get_field('image_3_title');
$image_3_description = get_field('image_3_description');
$image_4 = get_field('image_4');
$image_4_title = get_field('image_4_title');
$image_4_description = get_field('image_4_description');
$image_5 = get_field('image_5');
$image_5_title = get_field('image_5_title');
$image_5_description = get_field('image_5_description');
$title = get_field('title');
$sub_title = get_field('sub_title');

// create id attribute for specific styling
$id = 'museum-sections-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>
<div class="block-container museum-sections-container" id=<?=$id?>>
<p class="museum-sections-title"><?php echo $title ?></p>
<p class="museum-sections-sub-title"><?php echo $sub_title ?></p>
<div class="museum-sections-inner-container">
<div class="section-1">
<div class="section-text-wrap">
<p class="museum-section-image-title">
<?php echo $image_1_title ?>
</p>
<p class="museum-section-image-description">
<?php echo $image_1_description ?>
</p>
</div>
<img src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['alt']; ?>" />
</div>
<div class="section-2">
<img src="<?php echo $image_2['url']; ?>" alt="<?php echo $image_2['alt']; ?>" />
<div class="section-text-wrap">
<p class="museum-section-image-title">
<?php echo $image_2_title ?>
</p>
<p class="museum-section-image-description">
<?php echo $image_2_description ?>
</p>
</div>

</div>
<div class="section-3">
<div class="section-text-wrap">
<p class="museum-section-image-title">
<?php echo $image_3_title ?>
</p>
<p class="museum-section-image-description">
<?php echo $image_3_description ?>
</p>
</div>
<img src="<?php echo $image_3['url']; ?>" alt="<?php echo $image_3['alt']; ?>" />
</div>


<div class="section-4">
<img src="<?php echo $image_4['url']; ?>" alt="<?php echo $image_4['alt']; ?>" />
<div class="section-text-wrap">
<p class="museum-section-image-title">
<?php echo $image_4_title ?>
</p>
<p class="museum-section-image-description">
<?php echo $image_4_description ?>
</p>
</div>

</div>


<div class="section-5">
<div class="section-text-wrap">
<p class="museum-section-image-title">
<?php echo $image_5_title ?>
</p>
<p class="museum-section-image-description">
<?php echo $image_5_description ?>
</p>
</div>
<img src="<?php echo $image_5['url']; ?>" alt="<?php echo $image_5['alt']; ?>" />
</div>

</div>
</div>
