<?php
echo '<pre>';

$terms = get_terms('exhibitions_cat', array(
    'hide_empty' => false,
));

?>
<div class="block-container exhibitions-container">

<div class="categories-grid">
<?php

foreach ($terms as $term) {
    echo '<div class="exhibitions-category">';
    $images = get_field('image', $term);
    echo "<img src='$images[url];' class='$term->slug-image' alt=' $images[alt];' />";
    echo $term->name;
    echo '</div>';

}

wp_reset_postdata();
?>
</div>
</div>
