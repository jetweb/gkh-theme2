<?php
//echo '<pre>';
global $post;
/* $args = ['post_type' => 'visual_testimony', 'post_status' => 'publish'];
$the_query = new WP_Query($args);
 */

$terms = get_terms('photos_cat', array(
    'hide_empty' => false,
));
//print_r($terms);
?>


<div class="block-container visual-photos-container">

<div class="categories-grid">
<?php

foreach ($terms as $term) {
    echo '<div class="visual-photos-category">';
    $images = get_field('image', $term);

    echo "<img src='$images[url];' class='$term->slug-image' alt=' $images[alt];' />";
    echo $term->name;
    echo '</div>';

}
wp_reset_postdata();
?>
</div>
</div>
