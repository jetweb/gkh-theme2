<?php
$year = get_field('year');
$text = get_field('text');
?>
<div class="biography-block">
<p class="biography-text"><?php echo $text ?></p>
<p class="biography-year"><?php echo $year ?></p>

</div>