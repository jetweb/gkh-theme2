<?php
//echo '<pre>';

$post_info = get_field('news_article');
//var_dump($post_info);
$post_meta = get_post_meta($post_info->ID);
//var_dump($post_meta);
$image = wp_get_attachment_image($post_meta['image'][0], 'large');
?>

<div class="block-container home-news-article-container blockfull" >
<div class="block-inner-container home-news-inner flex-item" id='home-news-inner'>
<div class="news-text-wrap text-wrap">
<p class="news-article-pre-title pre-title">חדשות הגנזך</p>
<h2 class="news-article-title"><?php echo $post_info->post_title ?></h2>
<p class="news-article-summary"><?php echo $post_meta['summary'][0] ?></p>
<div class="button-container">
<a href="">קרא עוד</a>
<a href="">לכל החדשות</a>
</div>
</div>
<div class="news-image-wrap">
<?php echo $image ?>
</div>
</div>
</div>
