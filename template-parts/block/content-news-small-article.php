<?php
//echo '<pre>';
$article_1 = get_field('article_1');
$article_2 = get_field('article_2');
$article_3 = get_field('article_3');
$article_4 = get_field('article_4');
//var_dump($post_info);
$article_1_meta = get_post_meta($article_1->ID);
$article_2_meta = get_post_meta($article_2->ID);
$article_3_meta = get_post_meta($article_3->ID);
$article_4_meta = get_post_meta($article_4->ID);
//var_dump($post_meta);
$image_1 = wp_get_attachment_image($article_1_meta['image'][0], 'thumb');
$image_2 = wp_get_attachment_image($article_2_meta['image'][0], 'thumb');
$image_3 = wp_get_attachment_image($article_3_meta['image'][0], 'thumb');
$image_4 = wp_get_attachment_image($article_4_meta['image'][0], 'thumb');
?>

<div class="block-container news-small-article-container">
<p class="news-box-title">חדשות הגנזך</p>
<p class="news-box-sub-title">חדשות ועדכונים אחרונים בגנזך קידוש השם</p>
<div class="articles-wrap">

<div class="article_1">
<div class="article-image">
<?php echo $image_1 ?>
</div>
<p class="article-title">
 <?php echo $article_1->post_title ?>
</p>
<p class="read-more">לקריאה</p>
</div>

<div class="article_2">
<div class="article-image">
<?php echo $image_2 ?>
</div>
<p class="article-title">
 <?php echo $article_2->post_title ?>
</p>
<p class="read-more">לקריאה</p>
</div>

<div class="article_3">
<div class="article-image">
<?php echo $image_3 ?>
</div>
<p class="article-title">
 <?php echo $article_3->post_title ?>
</p>
<p class="read-more">לקריאה</p>
</div>

<div class="article_4">
<div class="article-image">
<?php echo $image_4 ?>
</div>
<p class="article-title">
 <?php echo $article_4->post_title ?>
</p>
<p class="read-more">לקריאה</p>
</div>

</div>
</div>
