<div class="memorial-banner">
<p class="memorial-banner-title">יזכור</p>
<p class="memorial-banner-text">הנציחו את שמות יקיריכם שניספו בשואה, את זכר קהילתכם שחרבה, את זכר מרכזי התורה והחסידות שנכחדו בשואה במרכז היהודי החדש</p>
<a href="" class="memorial-banner-button">תרומה והנצחה</a>
</div>
