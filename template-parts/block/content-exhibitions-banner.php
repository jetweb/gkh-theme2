<div class="block-container exhibitions-banner">
<p class="exhibitions-pre-title">אוצרות הגנזך</p>
<p class="exhibitions-title">תערוכות מקוונות</p>
<p class="exhibitions-text">תערוכות מקוונות בנושאים הקשורים להווי חיים יהודי מסורתי, בהתאם לצורכי החינוך והחברה
</p>
<a href="<?= get_field('exhibitions_page','options') ?>" class="exhibitions-link">לכל התערוכות</a>
</div>
