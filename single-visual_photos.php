<?php
add_action('genesis_entry_content', 'diplay_photo_gallery');
function diplay_photo_gallery()
{
    $gallery = get_field('images');
    // var_dump($gallery);
    ?>
<div class="block-container exhibitions-container blockfull">
    <div class="block-inner-container">
        <div class="exhibitions-grid">
            <?php
$num = 1;
    while (have_rows('images')): the_row();
        $image = get_sub_field('image');
        ?>
            <a data-fancybox='gallery' data-src="#hidden-content-<?php echo $num ?>" href="javascript:;"
                class="exhibition-item">
                <img src=" <?php echo $image['sizes']['single_photo_gallery'] ?>" alt="">
            </a>
            <div style="display: none;" class='modal' id="hidden-content-<?php echo $num ?>">
                <div class="flex-item">
                    <div class="modal-image-wrap">
                        <img src=" <?php echo $image['sizes']['large'] ?>" alt="">
                    </div>
                    <div class="modal-text-wrap">
                        <div class="modal-content">
                            <?php echo get_sub_field('desc'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
    $num++;
    endwhile;?>
        </div>
    </div>
</div>
<?php
}
genesis();